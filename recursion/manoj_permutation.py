import logging
logging.basicConfig(level=logging.DEBUG)

arr = ["a", "b", "c"]
# Expected result = ["abc", "acb", "cab", "cba", "bac", "bca"]
# No duplicates

result = []

def main_permute(arr):
    helper("", arr)
    logging.info(result)

def helper(slate, input):
    if len(input) == 0:
        logging.info("Appending slate: %s", slate)
        result.append(slate)
        return slate
    else:
        for j in range(0, len(input)):
            logging.info("j: %s | input: %s | input id: %s", j, input, id(input))
            helper(slate + input[j], input[:j]+input[j+1:])
            #helper(slate + input[0], input)
            #logging.info("Swapping index 0 with j")
            #input[0], input[j] = input[j], input[0]

main_permute(arr)

# Works but try to minimize space complexity
