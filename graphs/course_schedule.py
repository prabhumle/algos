"""
Course Schedule
There are n courses to take, they are referred to by numbers from 0 to n-1. Some of them have prerequisites, e.g. courses A and B must be completed before course C can be taken (in other words, course C depends on A and B).
Find and return an ordering in which all the given courses can be taken while satisfying all the prerequisites. If there exists more than one such ordering, any one of them would be a correct answer. If no such ordering exists, return a special value: [-1].
"""

import logging
logging.basicConfig(level=logging.DEBUG)


def course_schedule(n, prerequisites):

    # Prepare adjacency list
    #if len(prerequisites) != n:
    #    return [-1]

    adj_list = [ [] for _ in range(n) ]
    for course, prereq in prerequisites:
        # This is a directed graph, so the reverse is not true
        adj_list[prereq].append(course)
    logging.info("adj_list: %s", adj_list)

    visited = [-1] * n
    departures = [-1] * n
    arrivals = [-1] * n
    time = [-1]
    topsort = []

    # Ensure all the n courses can be completed without going into a cycle
    def dfs(node):
        logging.info("Visiting node: %s", node)
        visited[node] = 1

        time[0] += 1
        arrivals[node] = time[0]

        logging.info("Arrivals : %s", node)

        for neighbor in adj_list[node]:
            logging.info("Visiting neighbor: %s", neighbor)
            if visited[neighbor] == -1:
                visited[neighbor] = 1
                logging.info("Not seen this neighbor %s before", neighbor)
                if dfs(neighbor)[0] == -1:
                    return [-1]
            else:
                logging.info("This neighbor: %s is already visited", neighbor)
                if departures[neighbor] == -1:  # Back-edge return
                    return [-1]

        time[0] += 1
        departures[node] = time[0]
        topsort.append(node)  # Capture departures in the emptying stack order
        logging.info("arrivals: %s", arrivals)
        logging.info("Departures: %s", departures)
        logging.info("topsort: %s", topsort)
        return topsort

    # Outer loop
    for i in range(n):
        if visited[i] == -1:
            result = dfs(i)
            if result[0] == -1:
                break
    result.reverse()
    return result


#result = course_schedule(4, [[1, 0], [2, 0], [3, 1], [3, 2]])
result = course_schedule(3, [[0, 1], [1, 0]])
logging.info("results: %s", result)