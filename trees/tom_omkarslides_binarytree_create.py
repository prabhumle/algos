# The live class was from Tom Gilmour but the logic is from Omkar's PPT
# https://drive.google.com/file/d/1pyDINp9utJNG2uifRRGneRpg5Q5FPolu/view

import logging
logging.basicConfig(level=logging.DEBUG)


# The node creation logic is simply based on reference from https://www.geeksforgeeks.org/binary-tree-set-1-introduction/
class TreeNode(object):
    def __init__(self, key, data=None):
        self.key = key
        self.data = data
        self.left_child = None
        self.right_child = None


root = TreeNode(3)
root.left_child = TreeNode(9)
root.right_child = TreeNode(2)

# Add children to right child of root
root.right_child.left_child = TreeNode(15)
root.right_child.right_child = TreeNode(15)

