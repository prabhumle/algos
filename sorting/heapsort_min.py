# Min heap - minimum num will be at the root node in the binary tree

import heapq
import logging

logging.basicConfig(level=logging.DEBUG)

def heapify_min(arr):
    # heapify by default provides min heap
    heapq.heapify(arr)
    logging.info("Heapified arr")
    sorted_list = []
    while len(arr) > 0:
        item = heapq.heappop(arr)
        sorted_list.append(item)
        logging.info("%s", item)
    print(sorted_list)
    return sorted_list

heapify_min([5,8,1,2,7,3,4])

