# Complete the function below.

def generate_all_expressions(s, target):

    n = len(s)
    results = []

    def helper(i, preval, strrep, total):
        if i == n:
            if total == target:
                results.append(strrep)
            return
        else:
            if i == 0:
                helper(i+1, int(s[0]), s[0], int(s[0]))
            # Add
            helper(i+1, preval + int(s[i]), strrep + "+" + s[i], total+int(s[i]))

            # Str cat
            helper(i+1, int(str(preval) + s[i]), strrep + s[i], total + int(str(preval) + s[i]) )

    helper(0, int(s[0]), s[0], int(s[0]))
    print(results)
    return results

generate_all_expressions("222", 24)
generate_all_expressions("1234", 10)


