# The live class was from Tom Gilmour but the logic is from Omkar's PPT
# https://drive.google.com/file/d/1pyDINp9utJNG2uifRRGneRpg5Q5FPolu/view


# Leet#103 level order traversal zigzap print

import logging
logging.basicConfig(level=logging.DEBUG)


# The node creation logic is simply based on reference from https://www.geeksforgeeks.org/binary-tree-set-1-introduction/
class TreeNode(object):
    def __init__(self, key, data=None):
        self.key = key
        self.data = data
        self.left = None
        self.right = None


# Create a tree provided in the problem Leet#102
root = TreeNode(3)
root.left = TreeNode(9)
root.right = TreeNode(20)

# Add children to right child of root
root.right.left = TreeNode(15)
root.right.right = TreeNode(7)


# Solution
from collections import deque


def level_order_traversal(root):

    if root is None:
        return []

    result = []
    # Create a new queue with just the root
    q = deque([root])

    zigzag = -1

    while len(q) != 0:
        zigzag *= -1
        no_of_nodes = len(q)
        logging.info("Number of nodes/length of current queue: %s", no_of_nodes)
        same_level_nodes = []
        for _ in range(no_of_nodes):
            node = q.popleft()
            logging.info("Node value: %s", node.key)
            same_level_nodes.append(node.key)  # Append node to result

            # Check for children
            if node.left is not None:
                logging.info("left node is None")
                q.append(node.left)  # Enqueue

            if node.right is not None:
                logging.info("Right node is None")
                q.append(node.right) # Enqueue

        if zigzag == 1:
            result.append(same_level_nodes)
        else:
            reversed_list = []
            for i in range(len(same_level_nodes)-1, -1, -1):
                reversed_list.append(same_level_nodes[i])
            result.append(reversed_list)

    logging.info("result: %s", result)

level_order_traversal(root)

