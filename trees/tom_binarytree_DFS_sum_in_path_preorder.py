# The live class was from Tom Gilmour but the logic is from Omkar's PPT
# https://drive.google.com/file/d/1pyDINp9utJNG2uifRRGneRpg5Q5FPolu/view


# Leet#112 Pathsum Given tree and sum check if a root-to-leaf path creates a sum

import logging
logging.basicConfig(level=logging.DEBUG)


# The node creation logic is simply based on reference from https://www.geeksforgeeks.org/binary-tree-set-1-introduction/
class TreeNode(object):
    def __init__(self, key, data=None):
        self.key = key
        self.data = data
        self.left = None
        self.right = None


# Create a tree provided in the problem Leet#102
root = TreeNode(5)
root.left = TreeNode(4)
root.right = TreeNode(8)
root.left.left = TreeNode(11)
root.left.left.left = TreeNode(7)
root.left.left.right = TreeNode(2)

# Add children to right child of root
root.right.left = TreeNode(13)
root.right.right = TreeNode(4)
root.right.right.right = TreeNode(1)


# Solution
def dfs(node, expected_sum):
    if node is None:
        return

    flag = [False]

    def helper(node, expected_sum, current_sum):
        # If reached the leaf node
        current_sum = current_sum+node.key

        if node.left is None and node.right is None:
            if expected_sum == current_sum:
                flag[0] = True
            return
        else:
            if node.left:
                helper(node.left, expected_sum, current_sum)
            if node.right:
                helper(node.right, expected_sum, current_sum)

    helper(node, expected_sum, 0)
    logging.info("Result: %s", flag[0])



dfs(root, 22)

