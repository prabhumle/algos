# The live class was from Tom Gilmour but the logic is from Omkar's PPT
# https://drive.google.com/file/d/1pyDINp9utJNG2uifRRGneRpg5Q5FPolu/view


# Leet# 94 In a binary tree return inorder traversal of its nodes

import logging
logging.basicConfig(level=logging.DEBUG)


# The node creation logic is simply based on reference from https://www.geeksforgeeks.org/binary-tree-set-1-introduction/
class TreeNode(object):
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None


# Create a tree provided in the problem
"""
root = TreeNode(1)
root.right = TreeNode(2)
root.right.left = TreeNode(3)
"""
root = TreeNode(1)
root.left = TreeNode(2)
root.right = TreeNode(3)
root.left.left = TreeNode(4)
root.left.right = TreeNode(5)
root.right.left = TreeNode(6)
root.right.right = TreeNode(7)


# Solution
def dfs(node):
    if node is None:
        return 0

    inorder_traversal = []

    def helper(node):
        if node.left is None and node.right is None:
            # Entered leaf node - Do nothing
            logging.info("passing thru leaf node")
        if node.left:
            helper(node.left)
        inorder_traversal.append(node.val)
        if node.right:
            helper(node.right)
    helper(node)
    logging.info("result: %s", inorder_traversal)

dfs(root)

