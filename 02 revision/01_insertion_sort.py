# Think of a deck of cards where you have an already sorted list
# And then there is an unsorted list
# Now pick the first element from unsorted and insert the element into the sorted list in correct order, by comparing
# ..one element at a time

def insertion_sort(arr):
    """
    # i = Sorted array index
    # j = Unsorted array index
    """
    for j in range(1, len(arr)):
        i = j - 1  # Fetch topmost sorted elem
        # Take the new unsorted element and put it in the right spot in the sorted array
        while i >= 0 and arr[i] > arr[i+1]:
            arr[i], arr[i+1] = arr[i+1], arr[i]
            i -= 1
    return arr

#print(insertion_sort([7, 4, 3, 1, 2]))
#print(insertion_sort([7, 8, 8, 1, 2, 9, 0]))
#print(insertion_sort([]))
#print(insertion_sort([1]))


def recursive_insertion_sort(arr, i):
    """
    Write recursive logic here
    """
    if i >= len(arr):
        return arr

    if i >= 0 and i+1 < len(arr):
        if arr[i] > arr[i+1]:
            arr[i], arr[i+1] = arr[i+1], arr[i]
            recursive_insertion_sort(arr, i-1)
    recursive_insertion_sort(arr, i+1)

arr = [7, 8, 8, 1, 2, 9, 0]
recursive_insertion_sort(arr, 0)
print(arr)

arr = [7, 4, 3, 1, 2]
recursive_insertion_sort(arr, 0)
print(arr)

arr = [1]
recursive_insertion_sort(arr, 0)
print(arr)

arr =[]
recursive_insertion_sort(arr, 0)
print(arr)