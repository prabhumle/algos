# Like inserting a new card into existing cards in a sorted order
# Move from left to right - left will be the sorted array
# If a small number is found on the right, move previousl sorted array to insert the new
# Simple video example: geeksforgeeks: https://www.youtube.com/watch?v=OGzPmgsI-pQ

import logging
logging.basicConfig(level=logging.DEBUG)

def insertion_sort_iterative(arr):
    n = len(arr)
    i = 0
    while i < n-1:
        logging.info("Inside 1st while: %s", arr)
        j = i + 1
        jth = arr[j]  # Save the the jth element incase the left side sorted arr needs to be shifted right
        new_i = i
        logging.info("Inside 1st while: %s", arr)
        while arr[new_i] > jth and new_i >= 0:
            logging.info("Inside 2nd while: new_i: %s", new_i)
            logging.info("Inside 2nd while: arr: %s", arr)
            arr[new_i], arr[new_i + 1] = arr[new_i + 1], arr[new_i]
            new_i -= 1
            logging.info("Inside End 2nd while: arr: %s", arr)
        logging.info("Outside 2nd while: %s", arr)
        i += 1

input_arr = [5, 2, 3, 6, 1, 8, 7]
insertion_sort_iterative(input_arr)