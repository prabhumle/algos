
# Beautiful explanation: https://www.youtube.com/watch?v=Y0lT9Fck7qI
# To write the recurrence equation first
def count_ways_to_climb(steps, n):
    if n == 0:
        return 1
    # Derive recurrence formula
    """
    def helperRecurrenceFormula(value, steps):
        if n == value:
            return 1
        if value > n:
            return 0
        result = 0
        for step in steps:
            # The result is basically f(i+1) + f(i+2)
            result = result + helperRecurrenceFormula(value+step, steps)
        return result

    #print(helperRecurrenceFormula(0, steps))
    """

    # Now we understand that the final is going to be returned at f(0) which will have the sum of all subtrees
    # We also know the num of ways to reach target n from n is 1 (for example from step 5 to step 5 you count 1).
    # Similarly from step 4 you can reach step 5 only once using step 1 - using step 2 will result in 6 which incorrect target
    dp_table = [0] * (n+1)
    dp_table[n] = 1
    #print("dp_table:", dp_table)
    # We have already filled the n value so we can now fill the rest of the values
    # Bottom-up using a for-loop
    for i in range(n-1, -1, -1):
            sum_val = 0
            # Iterate over the step provided and sum them .. this will works for any num of steps
            for step in steps:
                #print("i:", i)
                if i+step > n:
                    step_paths = 0
                else:
                    step_paths = dp_table[i+step]
                sum_val += step_paths
            dp_table[i] = sum_val
    #print("dp_table: ", dp_table)
    return dp_table[0]  # Return 0 th step which contains the sum of all paths
#print(count_ways_to_climb([2, 3], 7))
########################################################
"""
def minCoinsRecursiveApproach(coinCnt, change_list):
    if coinCnt == 0:
        return 0

    if coinCnt < 0:
        return float('inf')

    min_coins = float('inf')
    for coin in change_list:
        min_coins = min(minCoinsRecursiveApproach(coinCnt-coin, change_list), min_coins)
    min_coins += 1
    return min_coins

#print(minCoinsRecursiveApproach(11, [1,2,5]))
#print(minCoinsRecursiveApproach(3, [1,2]))
#print(minCoinsRecursiveApproach(3, [2]))
print(minCoinsRecursiveApproach(9, [1, 3, 5]))
"""
# DP approach
# For details check image "DP_coin_change.png" in this git directory
def minimum_coins(coins, value):
    # Create dp table to reach value provided
    dp_table = [float('inf')] * (value + 1)  # +1 to include the initial 0 value

    # Let us set the default value that are available
    # The minimum steps to reach the values for the coins provided are themselves
    # For each - if coins = [1, 3, 5] then 1, 3, 5 themselves are the minimum coins required, so 1 step required to reach them
    for coin in coins:
        if coin > value:
            # Skip coin values greater than the reqd total value
            continue
        dp_table[coin] = 1
    # Let us include 0 value to begin with which is a 0 coins too
    dp_table[0] = 0

    # Starting from change coints required to get value 1 upto max value
    for i in range(1, value+1):
        # ignore already set base condition values - we don't want to alter these values
        if dp_table[i] != float('inf'):
            continue

        min_val = float('inf')
        # Branch for the multiple coins
        for coin in coins:
            if coin > value:
                # Skip coin values greater than the reqd total value
                continue
            # Add check to make sure that we don't fail the index range
            #print("i, coin:", i, ", ", coin)
            if i - coin >= 0:
                min_val = min(min_val, dp_table[i-coin])
            #print("min_val:", min_val)
        dp_table[i] = min_val + 1  # Add +1 to include the 1 step already taken before we find the min value subtree
    #print("dp_table:", dp_table)
    return dp_table[value]

#print(minimum_coins([1, 3, 5], 9))
#print(minimum_coins([1, 7, 22, 100, 64, 28, 8], 21))
##########################################
"""
def recurrence_can_reach_last_house(maximum_jump_lengths):
    target_house = len(maximum_jump_lengths)
    def helper(curr_house):
        print("curr_house:", curr_house)
        if curr_house == target_house:
            return True
        if curr_house < target_house:
            print("maximum_jump_lengths[curr_house]:", maximum_jump_lengths[curr_house])
            available_jumps = range(1, maximum_jump_lengths[curr_house]+1)
            jump_success = False
            for jump in available_jumps:
                print("jumping:", jump)
                jump_success = jump_success or helper(curr_house+jump)
            return jump_success
        else:
            # Can't jump
            print("Can't jump: curr_house > target_house")
            return False
    return helper(0)
#print(recurrence_can_reach_last_house([2, 3, 1, 0, 4, 7]))
#print(recurrence_can_reach_last_house([3, 1, 1, 0, 2, 4]))
"""

# DP solution
# Based on the recurrence formula we saw above,
# The base condition is that if we reach the last house we are True which mean dp[tgt_house] = True
# Now, if we can go one step at a time tgt_house-1, tgt_house-2... until 0 (setting house value True or False) based on successful jum
# We can get the value from 0th index (by other words we can reach the 0th index from the target house)
def can_reach_last_house(maximum_jump_lengths):
    """
    Args:
     maximum_jump_lengths(list_int32)
    Returns:
     bool
    """
    # Write your code here.
    actual_target_house = len(maximum_jump_lengths)
    dp = [-1] * actual_target_house
    dp[actual_target_house-1] = True

    for given_target_house in range(actual_target_house-1, -1, -1):
        if given_target_house == actual_target_house-1:
            # Already set
            continue
        #print("given_target_house:", given_target_house)
        available_jumps = maximum_jump_lengths[given_target_house]
        #print("available_jumps:", available_jumps)
        jump_success = False
        if available_jumps > 0:
            for jump in range(1, available_jumps+1):
                if given_target_house+jump <= actual_target_house-1:
                    jump_success = jump_success or dp[given_target_house+jump]
        dp[given_target_house] = jump_success
    #print("dp:", dp)
    return dp[0]

#print(can_reach_last_house([2, 3, 1, 0, 4, 7]))
#print(can_reach_last_house([3, 1, 1, 0, 2, 4]))
#######
"""
# The assumption for this solution is that the thief is always going from left to right. Else the solution is diffeerent
def recurrence_maximum_stolen_value(values):
    # dfs
    n = len(values)

    def dfs(i):
        max_parent_value = 0
        for start_house in range(i, n):
            max_child_value = 0
            if start_house + 2 <= (n-1):
                for neighbor_house in range(start_house + 2, n):
                    max_child_value = max(max_child_value, dfs(neighbor_house))
            max_parent_value = max(max_parent_value, max_child_value+values[start_house])
        return max_parent_value
    return dfs(0)
#print(recurrence_maximum_stolen_value([6, 1, 2, 7]))
#print(recurrence_maximum_stolen_value([1, 8, 3, 1, 2, 9, 1, 6]))

# DP
# The assumption for this solution is that the thief is always going from left to right. Else the solution is diffeerent
# Based on example tree using [6, 1, 2, 7] we created &
# From the above recurrence relation we know that the base condition, is when there are no neighbors (last couple of houses)
# to jump to, also
# The max values for the houses itself were max_of_parents( parent_house_value + max(children))
# Check diagram in this folder for approach(dp_robbery_solution_approach.jpeg)
def nonoptimal_maximum_stolen_value(values):
    n = len(values)
    # Create DP values with max value at a particular house
    dp = [0] * (n)  # To include 0 as the starting point. Index 0 will have the max values amongst all houses robbed from/final

    # We will go in the reverse order as the base condition lies is the last house with no neighbors to jump to
    for i in range(n-1, -1, -1):
        if i == n-1:
            # The max value we have at the beginning is the house itself (no children/neighbor houses to the right)
            dp[i] = values[i]
            #print("dp:", dp)
        else:
            # Get max of children
            max_of_child = 0
            if i+2 <= n-1:  # Check to ensure that next possible childs are within range
                for child in range(i+2, n):
                    max_of_child = max(max_of_child, dp[child])  # Choose the max available value among children
            # Get max of previous parent and current parent
            dp[i] = max(dp[i+1], max_of_child + values[i])  # Choose the maximum among known parents and current parents
    #print("dp:", dp)
    return dp[0]

#print(nonoptimal_maximum_stolen_value([6, 1, 2, 7]))
#print(nonoptimal_maximum_stolen_value([1, 8, 3, 1, 2, 9, 1, 6]))
"""
# DP - optimal solution
# Here we how left to right - to see what is the maximum value at f(n) (f(n) being the last house robbed)
# so, for f(n) - you would NOT include "n" if the previous house was robbed (per adjacent house condition), and
# you will include "n" if the previous house was NOT robbed then  the sum robbed would be (f(n-1) + values[n])
# So, finally the max_value that can be robbed at f(n) is the max value by including or excluding the house
# Therefore: f(n) = MAX(f(n-1), f(n-2)+values[n]) ; result will at f(n)
def maximum_stolen_value(values):
    """
    Args:
     values(list_int32)
    Returns:
     int32
    """
    n = len(values)
    dp = [0] * (n+1)  # Include 0 as the starting house
    dp[0] = 0  # Value of the starting house
    dp[1] = values[0]  # The first house robbed will only have the actual value of the house itself

    for i in range(2, n+1):
        # f(n) = MAX(f(n-1), f(n-2)+values[n])
        dp[i] = max(dp[i-1], dp[i-2] + values[i-1])  # Note: values length is 1 value less than the dp list
    return dp[n]

#print(maximum_stolen_value([6, 1, 2, 7]))
#print(maximum_stolen_value([1, 8, 3, 1, 2, 9, 1, 6]))
###############

def levenshtein_distance(word1, word2):
    """
    Args:
     word1(str)
     word2(str)
    Returns:
     int32
    """
    # Write your code here.
    """
    We will start with addiontal index 0 added
    i: 0 C A T
    j: 0 B A L L
    
    # If there is w1[i] == w2[j], there is no change required
    #
    # Else, if there is an update, insert & delete results in making one change
    # The following task being passed by the parent task to be passed to the child task
    # Insert: w1[i], w2[j+1]  (this inserts the element from w2 into w1)  
    # Update: w1[i+1], w2[j+1] (takes one element from w2 and updates w1)
    # Delete: w1[i+1], w2[j]  (deletes a char from w1)
    # So, the resulting change will be (1 + min((w1[i], w2[j+1]),  (w1[i+1], w2[j+1]), (w1[i+1], w2[j])))
    # This is nothing but the current node's change + min change required from the children
    
    # Base condition population
      # B A L L 
     ___________
    #|0|1|2|3|4|
    ____________
    C|1| | | | |
    ____________
    A|2| | | | |
    ____________
    T|3| | | | |
    ____________
    
    
    """
    w1 = len(word1)
    w2 = len(word2)



    # See image for base condition logic
    # Base conditions
    # If a node has w1 = 0 chars to change but w2=0 or more chars
    # OR
    # If a node has w2 = 0 chars to change but w1=0 or more chars
    dp = [[0] * (w2+1) for _ in range(w1+1)]
    for i in range(w1+1):
        dp[i][0] = i
    for j in range(w2+1):
        dp[0][j] = j

    for i in range(1, w1+1):
        for j in range(1, w2+1):
            if word1[i-1] == word2[j-1]:
                dp[i][j] = dp[i-1][j-1]
            else:
                dp[i][j] = 1 + min(dp[i-1][j], dp[i-1][j-1], dp[i][j-1])
    print(dp)
    return dp[w1][w2]

#print(levenshtein_distance("cat", "bat"))
######################################################

def count_phone_numbers_of_given_length(startdigit, phonenumberlength):
    """
    Args:
     startdigit(int32)
     phonenumberlength(int32)
    Returns:
     int64

    For phone key pad based on knights' movement the following with be paths:
    neighbors = {
        0:(4,6),
        1:(6,8),
        2:(7,9),
        3:(4,8),
        4:(0,3,9),
        5:(),
        6:(0,1,7),
        7:(2,6),
        8:(1,3),
        9:(2,4)
    }
    Let's assume the start digit is 2 and the  phoneno_len is 3

             (2)  --> f(2,3)
            /    \
f(7,2)--> (7)     (9)  <-- f(9,2)
          / \     /  \
         (2) (6) (2) (4) --> f(4,1)
    f(2, 1)f(6,1) f(2,1)

    Now, let us construct DP table intuitively
                    Keypad numbers
    phoneno length |0|1|2|3|4|5|6|7|8|9|
                   ---------------------
                  0|0|0|0|0|0|0|0|0|0|0|  --> no number is needed (this row is not really used - optional)
                  1|1|1|1|1|1|1|1|1|1|1|  --> base conditions (for length 1, the result will be the number itself)
                  2|2|2|2|2|3|0|3|2|2|2|  --> sum of my neighbors/child - we populate this row iteratively by checking row above based on neightbors list
                  3|6|5|4|5|6|0|5|5|4|5|  --> We will use the same idea we used for length#2 above to populate this
    So, in this example f(2,3) will be 4
    """
    # Write your code here.
    neighbors = {
        0:(4,6),
        1:(6,8),
        2:(7,9),
        3:(4,8),
        4:(0,3,9),
        5:(),
        6:(0,1,7),
        7:(2,6),
        8:(1,3),
        9:(2,4)
    }
    # Let us build the dp table
    n = phonenumberlength + 1
    dp = [[0]*10 for _ in range(n)]
    # Populate base conditions
    for i in range(10):
        dp[0][i] = 0
    for i in range(10):
        dp[1][i] = 1

    for i in range(2, n):
        for j in range(10):
            if j == 5 and i != 1:
                dp[i][j] = 0
            else:
                dp[i][j] = sum([dp[i-1][neighbor] for neighbor in neighbors[j]])
    return dp[phonenumberlength][startdigit]

###################################################################################
def word_break_count(dictionary, txt):
    """
    Args:
     dictionary(list_str)
     txt(str)
    Returns:
     int32

    Considering the example provided
    {
    "dictionary": ["is", "a", "isac", "at", "i", "sa", "cat"],
    "txt": "isacat"
    }
    n = len(txt)  # 6

    - The proceed from 1st char to the last character in the first step (f(0,0), f(n,n))
    - The child will return 0 if no match is found in dictionary, it will return 1 + childvalue if match is found
    - The recursion depth will be n,, so overall the time complexity is 0(n^n) (oops that's exponential)

    The recurrence graphs looks something like this


                                     starting point
                /                         |          \          \            \            \
             f(0, 0) -->matches "i"      f(0,1)        f(0,2)     f(0, 3)      f(0, 4)     f(0, 5)
            /    \
        f(1,0) f(1,1) f(1,2)..f(1,5) --->   f(1,2) matches "sa"
                      /
                     f(3,5) --> matches "cat" (We will return 1 and stop here for this iteration because we reached depth "n" - 6 here)

    Now, let us construct a DP table from bottom up (child to parent) - because the parent is going to have sum of childs

    We will create a DP table of size 7 X 7 to include the parent (0,0) we introduced
    We can initialize the lengths zero for now

     |0|1|2|3|4|5|6|
    -----------------------
    0|0|0|0|0|0|0|0|
    1|0|0|0|0|0|0|0|
    2|0|0|0|0|0|0|0|
    3|0|0|0|0|0|0|0|
    4|0|0|0|0|0|0|0|
    5|0|0|0|0|0|0|0|
    6|0|0|0|0|0|0|0|
                <---(6,6)
    Now will start from the right-bottom-most child and fill the way up to the parent
    """
    # Convert dict to hmap for O(1) operations
    word_dict = {key: 1 for key in dictionary}

    n = len(txt)
    # Let us create a dp table with size 7*7
    dp = [[0]*(n+1) for _ in range(n+1)]

    # Update base condition -- this is automatically set bcos of above initial dp setup
    # for col in range(n+1):
    #   dp[n][col] = 0

    for i in range(n-1, -1, -1):
        # We will set this value here once for every row
        #print("i start:", i)
        for j in range(i, n):
            print("j:", j)
            #print("Checking txt[i:j] in word_dict:", txt[i:j+1])
            if txt[i:j+1] in word_dict:
                #print("txt[i:j+1] in word_dict:", txt[i:j+1])
                #print("j+1:", j+1)

                # Base condition - when find end of string is reached, we add one for the leaf node
                if j+1 == n:
                    #print("j+1 == n")
                    dp[i][j] = 1
                else:
                    # Based on dp_Word_break.jpeg diagram, we can see the current node is the sum of the child nodes
                    dp[i][j] = sum(dp[j+1][j+1:])

            for r in range(len(dp)):
                print(dp[r])
    return sum(dp[0])

print(word_break_count(["is", "a", "isac", "at", "i", "sa", "cat"], "isacat"))
#print(word_break_count(["kick", "start", "kickstart", "is", "awe", "some", "awesome"], "kickstartisawesome"))