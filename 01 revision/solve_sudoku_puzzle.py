


def solve_sudoku_puzzle(board):
    results = []
    n = len(board)
    def helper(i, slate):
        if i == n:
            results.append(slate[:])
        else:
            for j in range(i, n):
                if board[i][j] != 0:
                    slate.append(board[i][j])
                else:
                    if (j+1) in board[i][:]:
                        continue
                    slate.append(j+1)
                helper(i+1, slate)
                slate.pop()

    helper(0, [])
    print(results)
    return results


board = [
    [8, 4, 9, 0, 0, 3, 5, 7, 0],
    [0, 1, 0, 0, 0, 0, 0, 0, 0],
    [7, 0, 0, 0, 9, 0, 0, 8, 3],
    [0, 0, 0, 9, 4, 6, 7, 0, 0],
    [0, 8, 0, 0, 5, 0, 0, 4, 0],
    [0, 0, 6, 8, 7, 2, 0, 0, 0],
    [5, 7, 0, 0, 1, 0, 0, 0, 4],
    [0, 0, 0, 0, 0, 0, 0, 1, 0],
    [0, 2, 1, 7, 0, 0, 8, 6, 5]
]
solve_sudoku_puzzle(board)