
from collections import deque

def getNeighbours(x, y, max_x, max_y):
    # For this question we need to include the diagonals too
    # The maximum neighbors possible are 8 unless they are border rows/columns
    print("In (x, y): ", str(x) + "," + str(y))
    print("max_x & max_y: ", str(max_x) + " & " + str(max_y))
    neighbours = []
    if x-1 >= 0:
        neighbours.append((x-1, y))
    if y-1 >= 0:
        neighbours.append((x, y-1))

    if x+1 <= max_x:
        neighbours.append((x+1, y))
    if y+1 <= max_y:
        neighbours.append((x, y+1))

    # Diagonal values
    if x-1 >= 0 and y-1 >= 0:
        neighbours.append((x-1, y-1))
    if x+1 <= max_x and y+1 <= max_y:
        neighbours.append((x+1, y+1))

    if x-1 >= 0 and y+1 <= max_y:
        neighbours.append((x-1, y+1))
    if x+1 <= max_x and y-1 >= 0:
        neighbours.append((x+1, y-1))
    return neighbours

def bfs(x_axis, y_axis, matrix):
    q = deque([(x_axis, y_axis)])
    # Mark as visited
    matrix[x_axis][y_axis] = 999

    while len(q) > 0:
        vx, vy = q.popleft()
        print("Fetching neighbors for vx, vy:", str(vx) + "," + str(vy))
        neighbourList = getNeighbours(vx, vy, len(matrix)-1, len(matrix[0])-1)
        print("neighbourList:", neighbourList)
        for neigh_x, neigh_y in neighbourList:
            if matrix[neigh_x][neigh_y] != 0 and matrix[neigh_x][neigh_y] != 999:  # Not visited
                matrix[neigh_x][neigh_y] = 999
                q.append((neigh_x, neigh_y))


def count_islands(matrix):
    """
    Args:
     matrix(list_list_int32)
    Returns:
     int32
    """
    # Let try a BFS first
    islands = 0  # components

    for i in range(0, len(matrix)):
        for j in range(0, len(matrix[0])):
            if matrix[i][j] != 0 and matrix[i][j] != 999:
                bfs(i, j, matrix)
                islands += 1

    return islands

matrix = [
    [1, 1, 0, 0, 0],
    [0, 1, 0, 0, 1],
    [1, 0, 0, 1, 1],
    [0, 0, 0, 0, 0],
    [1, 0, 1, 0, 1]
]
# Call BFS
#print(count_islands(matrix))


def dfs(x_axis, y_axis, matrix):
    # Mark as visited
    matrix[x_axis][y_axis] = 999
    print("Fetching neighbors for x_axis, y_axis:", str(x_axis) + "," + str(y_axis))
    neighbourList = getNeighbours(x_axis, y_axis, len(matrix)-1, len(matrix[0])-1)
    print("neighbourList:", neighbourList)
    for neigh_x, neigh_y in neighbourList:
        if matrix[neigh_x][neigh_y] != 0 and matrix[neigh_x][neigh_y] != 999:  # Not visited
            dfs(neigh_x, neigh_y, matrix)

def count_islands_dfs(matrix):
    """
    Args:
     matrix(list_list_int32)
    Returns:
     int32
    """
    # Let try a BFS first
    islands = 0  # components

    for i in range(0, len(matrix)):
        for j in range(0, len(matrix[0])):
            if matrix[i][j] != 0 and matrix[i][j] != 999:
                dfs(i, j, matrix)
                islands += 1
    return islands

#print(count_islands_dfs(matrix))
#######################


def get_neighbors(x, y, max_x, max_y):
    neighbors = []

    if x-1 >= 0:
        neighbors.append((x-1, y))
    if y-1 >= 0:
        neighbors.append((x, y-1))
    if x+1 <= max_x:
        neighbors.append((x+1, y))
    if y+1 <= max_y:
        neighbors.append((x, y+1))
    print("neighbors: ", neighbors)
    return neighbors


def dfs_max_island_size(x_axis, y_axis, max_x, max_y, sum_count, grid):
    print("x_axis, y_axis: ", x_axis, " ", y_axis)
    grid[x_axis][y_axis] = 0
    sum_count += 1
    neighbor_list = get_neighbors(x_axis, y_axis, max_x, max_y)
    #print("Found neighbors:", neighbor_list)
    for neighbor_x,  neighbor_y in neighbor_list:
        if grid[neighbor_x][neighbor_y] == 1:
            print("Discovered neighbor_x, neighbor_y: ", neighbor_x, " ", neighbor_y)
            sum_count = dfs_max_island_size(neighbor_x, neighbor_y, max_x, max_y, sum_count, grid)
        else:
            print("Not land - neighbor_x, neighbor_y: ", neighbor_x, " ", neighbor_y)
    return sum_count


def max_island_size(grid):
    """
    Args:r
     grid(list_list_int32)
    Returns:
     int32
    """
    max_x = len(grid)-1
    max_y = len(grid[0])-1

    # Outer loop
    max_count = 0
    for i in range(0, max_x+1):
        for j in range(0, max_y+1):
            if grid[i][j] == 1:
                print("Starting graph at: i, j:", i, " ", j)
                count = dfs_max_island_size(i, j, max_x, max_y, 0, grid)
                if count > max_count:
                    max_count = count
    # Write your code here.
    return max_count

grid = [
    [0, 0, 0, 1],
    [1, 1, 1, 0],
    [0, 1, 1, 0]
]
#print(max_island_size(grid))
#####################################################
def create_adj_list(n, edges):
    adj_list = []
    for _ in range(0, n):
        adj_list.append([])
    for source, target in edges:
        # For undirected, add both sides
        adj_list[source].append(target)
        adj_list[target].append(source)
    return adj_list


def number_of_connected_components(n, edges):
    """
    Args:
     n(int32)
     edges(list_list_int32)
    Returns:
     int32
    """
    # Write your code here.
    visited = [-1] * n
    adjacency_list = create_adj_list(n, edges)
    print("adjacency_list:", adjacency_list)

    def dfs(i):
        visited[i] = 1
        print("i:", i)
        print("Current visited: ", visited)
        for node in adjacency_list[i]:
            if visited[node] != 1:
                dfs(node)

    # Outer loop to check separate components
    components = 0
    for x in range(0, n):
        if visited[x] != 1:
            components += 1
            dfs(x)
    return components

n = 5
edges = [[0 ,1], [1, 2], [0, 2], [3, 4]]
#print(number_of_connected_components(n, edges))
###########################################

# A tree has all nodes connected (no multiple components)
# A tree must have no cycle between the connected nodes


def create_adj_list(nodes, start_arr, end_arr):
    # both the input arrs are expected to be equal in length
    adj_list = [[] for i in range(0, nodes)]
    for i in range(0, len(start_arr)):
        # This is an undirected graph so add both ways
        adj_list[start_arr[i]].append(end_arr[i])
        adj_list[end_arr[i]].append(start_arr[i])
    print("adj_list:", adj_list)
    return adj_list

def is_it_a_tree(node_count, edge_start, edge_end):
    """
    Args:
     node_count(int32)
     edge_start(list_int32)
     edge_end(list_int32)
    Returns:
     bool
    """
    visited = [-1] * node_count
    parent = [-1] * node_count
    adj_list = create_adj_list(node_count, edge_start, edge_end)

    def dfs(node):
        visited[node] = 1
        neighbor_list = adj_list[node]
        for neighbor in neighbor_list:
            # Check if the neighbor has already been visited
            if visited[neighbor] == -1:
                # Add the node as the parent of the neighbor
                parent[neighbor] = node
                if not dfs(neighbor):
                    return False
            else:
                if parent[node] != neighbor:
                    return False
        return True


    # Outer loop
    components = 0
    for node in range(node_count):
        if visited[node] == -1:
            print("Outer loop: Node not yet visited:", node)
            components += 1
            # Tree should have just one connected component
            if components > 1:
                return False
            # Check tree if there is a cross edge or a back edge except for a parent
            if not dfs(node):
                return False
    return True
#print(is_it_a_tree(4, [0, 0], [1, 2]))

#############################################

def create_adj_list_directed(num_nodes, arr_a, arr_b):
    adj_list = [[] for _ in range(num_nodes)]
    for i in range(len(arr_a)):  # arr_a and arr_b are of the same length
        # This is a directed graph so  must be linked in one direction only
        adj_list[arr_a[i]].append(arr_b[i])
    print("adj_list:", adj_list)
    return adj_list


def can_be_completed(n, a, b):
    """
    Args:
     n(int32)
     a(list_int32)
     b(list_int32)
    Returns:
     bool
    """
    # Constraints
    # Directed graph
    # Complete all courses but make sure to complete prerequisites first
    # So, basically all visited vertices should be true not -1
    # There also should not any cycles
    # We will decide is a DIRECTED graph has cycles are not using DFS with arrirval and departure time (bookkeeping)

    # create adj_list
    adj_list = create_adj_list_directed(n, a, b)

    # Create visited, arrival, departure arrs
    visited = [-1] * n
    arrival = [-1] * n
    departure = [-1] * n
    timestamp = 0

    # Call dfs with bookkeeping (false if cycles exist)
    def dfs(node):
        nonlocal timestamp
        timestamp += 1
        arrival[node] = timestamp
        visited[node] = 1
        for neighbor in adj_list[node]:
            if visited[neighbor] == -1:
                # Tree node because it's not visited
                if not dfs(neighbor):
                    return False
            else:
                # This is probably a cross edge or back-edge, so check for back
                # Already visited check if there is a backedge (check for departure times not set - for the target ndoe)
                if departure[neighbor] == -1:
                    return False
        timestamp += 1
        departure[node] = timestamp
        return True


    # Outer loop - check from each node if all the vertices can be visited without creating cycles
    for i in range(n):
        if visited[i] == -1:  # We could just use the arrival node for this w/o visited but trying wiht it
            if not dfs(i):
                return False
    return True

#print(can_be_completed(4, [1, 1, 3], [0, 2, 1]))
##############################################
# The problem is a undirected graph because the dislike could be from both the parties

# We will solve the problem by coloring the alternate visited edges in blue and green
# If the alternate edge is of the same color we will reture False else True
# Having cycle (since dfs - cross or back edge) is ok

def create_dislike_adj_list(n, d1, d2):
    adj_list = [[] for _ in range(n)]
    for i in range(len(d1)):
        # This is an undirected graph
        adj_list[d1[i]].append(d2[i])
        adj_list[d2[i]].append(d1[i])
    #print("adj_list:", adj_list)
    return adj_list

def can_be_divided(num_of_people, dislike1, dislike2):
    """
    Args:
     num_of_people(int32)
     dislike1(list_int32)
     dislike2(list_int32)
    Returns:
     bool
    """
    adj_list = create_dislike_adj_list(num_of_people, dislike1, dislike2)
    visited = ["N"] * num_of_people

    # DFS (with check for back & cross edges)
    def dfs(node):
        #print("Processing node:", node)
        #print("visited:", visited)
        for neighbor in adj_list[node]:
            if visited[neighbor] == "N":
                # Rotate alternate color switcher for the next vertex
                if visited[node] == "B":
                    visited[neighbor] = "G"
                else:
                    visited[neighbor] = "B"
                # Tree node
                if not dfs(neighbor):
                    return False
            else:
                # check cross edge or back edge are having same colors
                if visited[neighbor] == visited[node]:
                    #print("Error:", visited[neighbor], neighbor, " connected to ", visited[node], node)
                    return False
        return True

    # Outer loop - to run on multiple connected components if any
    for i in range(num_of_people):
        if visited[i] == "N":
            if not dfs(i):
                return False
    return True

#print(can_be_divided(8, [0, 3, 4, 5, 3, 3], [1, 4, 5, 6, 6, 7]))
######################################################


def get_neighbors(x, y, matrix):
    adj_list = []
    if x - 1 >= 0:
        adj_list.append((x-1, y))
    if y - 1 >= 0:
        adj_list.append((x, y-1))
    if x + 1 <= len(matrix)-1:
        adj_list.append((x+1, y))
    if y + 1 <= len(matrix[0])-1:
        adj_list.append((x, y+1))
    print("adj_list:", adj_list)
    return adj_list


def flood_fill(pixel_row, pixel_column, new_color, image):
    """
    Args:
     pixel_row(int32)
     pixel_column(int32)
     new_color(int32)
     image(list_list_int32)
    Returns:
     list_list_int32
    """
    current_color = image[pixel_row][pixel_column]

    def dfs(i, j):
        print("i j:", i, " ", j)
        print("curr color:", image[i][j])
        image[i][j] = new_color
        for neighbor_x, neighbor_y in get_neighbors(i, j, image):
            if image[neighbor_x][neighbor_y] == current_color:
                dfs(neighbor_x, neighbor_y)

    # Outer loop for components
    # We know were to start using the starting position
    dfs(pixel_row, pixel_column)

    return image

image =  [
    [0, 2, 1],
    [1, 1, 2],
    [2, 5, 4]
]
#print(flood_fill(1, 0, 9, image))
###############################################

from collections import deque
def zombie_cluster(zombies):
    """
    Args:
     zombies(list_str)
    Returns:
     int32
    """
    # This problem needs a different approach compared to usual adjacent list because
    # there is basically the same half replica of the flags across the diagonal (because z0, z1,.. etc are repeated)
    # So, we will just loop across one half of the diagonal (propagate each row horizontally only)
    # the diagonals are all populated as per the problem statement - so we will start from the first elem (0,0)

    # A BFS based approach is more intuitive for this problem as we go line by line (but getting timeout error in Uplevel Full test for 1 test)
    len_zombies = len(zombies)
    visited = [-1] * len_zombies
    q = deque([])  # If using bfs

    def bfs(node):
        while q:
            #print("q: ", q)
            node = q.pop()
            # Walk through that row and
            for neighbor_zombie in range(len_zombies):
                if visited[neighbor_zombie] != 1 and zombies[node][neighbor_zombie] == "1":
                    # Mark that row as visited
                    visited[node] = 1
                    q.append(neighbor_zombie)

    def dfs(node):
        visited[node] = 1
        # Get horizontal neighbors
        for y in range(len_zombies):
            # See if they are infected
            if visited[y] == -1 and zombies[node][y] == "1":
                dfs(y)

    # Outer loop
    components = 0
    for x in range(len_zombies):
        if visited[x] == -1:
            components += 1
            dfs(x)
            """
            # BFS
            visited[x] = 1
            q.append(x)
            # Mark that row as visited
            bfs(x)
            """
    return components

from zombies import zombies
#zombies = ["1100", "1110", "0110", "0001"]
#print(zombie_cluster(zombies))
###########################################
from collections import deque

# Complete the function below.

def get_posssible_knights_paths(x, y, max_rows, max_cols):
    # Knights can move total of 3 steps (if there is no border ending in that direction)
    # either 1 or 2 steps above current positon, and 1 or 2 moves left or right depending on the current pos
    print("x,y - maxrows, maxcols:", x, ",", y, " - ", max_rows, ", ", max_cols)
    neighbour_list = []
    # Move two steps above and one step to the left
    if x-2 >= 0 and y-1 >= 0:
        neighbour_list.append((x-2, y-1))
    # Move one step above and two steps to the left
    if x-1 >= 0 and y-2 >= 0:
        neighbour_list.append((x-1, y-2))
    # Move two steps above and one step to the right
    if x-2 >= 0 and y+1 <= max_cols:
        neighbour_list.append((x-2, y+1))
    # Move one step above and two steps to the right
    if x-1 >= 0 and y+2 <= max_cols:
        neighbour_list.append((x-1, y+2))


    # Move two steps below and one step to the left
    if x+2 <= max_rows and y-1 >= 0:
        neighbour_list.append((x+2, y-1))
    # Move one step below and two steps to the left
    if x+1 <= max_rows and y-2 >= 0:
        neighbour_list.append((x+1, y-2))
    # Move two steps below and one step to the right
    if x+2 <= max_rows and y+1 <= max_cols:
        neighbour_list.append((x+2, y+1))
    # Move one step below and two steps to the right
    if x+1 <= max_rows and y+2 <= max_cols:
        neighbour_list.append((x+1, y+2))
    print("neigbor list: ", neighbour_list)
    return neighbour_list


def find_minimum_number_of_moves(rows, cols, start_row, start_col, end_row, end_col):
    # Write your code here.
    # The question is looking for the minimum number of moves needed to reach (shortest path) - so we will use BFS
    # There is no need for outer loop because we know the starting point

    chessboard = [ [-1] * cols for i in range(rows)]
    print("chessboard:", chessboard)
    q = deque([(start_row, start_col)])
    chessboard[start_row][start_col] = 1
    max_rows = rows-1
    max_cols = cols-1

    total_moves = -1  # Since first move is current position
    reached_dest = False
    while q and not reached_dest:
        q_length = len(q)
        total_moves += 1
        print("total_moves:  ", total_moves)

        while q_length > 0:
            node_x, node_y = q.popleft()
            if node_x == end_row and node_y == end_col:
                print("reached desitnation - break here")
                reached_dest = True
                break
            for neighbor_x, neighbor_y in get_posssible_knights_paths(node_x, node_y, max_rows, max_cols):
                if chessboard[neighbor_x][neighbor_y] != 1:
                    q.append((neighbor_x, neighbor_y))
                    chessboard[neighbor_x][neighbor_y] = 1

            q_length -= 1

    if reached_dest:
        return total_moves
    return -1  # Not able to reach end

#print(find_minimum_number_of_moves(5, 5, 0, 0, 4, 1))
#########################


# This will be a directed graph. There could be more than one connected components
# For a directed graphs detecting the type of cycle with bfs is challenging so we will use DFS with book-keeping(arrival+departure time)
# We can break from the loop if there is a cycle (which means no way to complete prerequisite)

def create_course_adj_list(num, prereq):
    adj_list = [[] for _ in range(num)]
    for course, prereqcourse in prereq:
        # This is a directed graph, so will do oneway
        adj_list[prereqcourse].append(course)
    return adj_list

def course_schedule(n, prerequisites):
    """
    Args:
     n(int32)
     prerequisites(list_list_int32)
    Returns:
     list_int32
    """
    visited = [-1] * n
    arrival = [-1] * n
    departure = [-1] * n
    timestamp = 0  # To keep track of arrival and departure times in recursion
    min_to_max_departure_times = []
    adj_list = create_course_adj_list(n, prerequisites)

    def dfs(node):
        nonlocal timestamp
        timestamp += 1
        arrival[node] = timestamp
        visited[node] = 1
        for neighbor in adj_list[node]:
            if visited[neighbor] == -1:
                # Launch dfs on the neighbor
                if not dfs(neighbor):
                    return False
            else:
                # Neighbor already visited
                # Check for cycle (back-edge) - departure time not set
                if departure[neighbor] == -1:
                    return False
        timestamp += 1
        departure[node] = timestamp
        # Capture the departure times of the nodes from min to max. This will hold the reverse order of completion
        # We can reverse this when we return the results
        min_to_max_departure_times.append(node)
        return True

    # Outerloop to find connected components (in this case cross-edge components)
    for i in range(n):
        if visited[i] == -1:
            if not dfs(i):
                return [-1]

    # reverse min_to_max_departure_times so that we can arrange courses from left to right
    #min_to_max_departure_times.reverse()
    return min_to_max_departure_times

#print(course_schedule(4, [[1, 0], [2, 0], [3, 1], [3, 2]]))
###################################################

# The question talks about finding the shortest path, so we will use BFS
# The question says that there is a solution, so, there will be no cycles?
# This can be an undirected graph considering " It is allowed to visit a cell more than once."
# There is no need for checking multiple components because we know the starting point


# https://zunayed.dev/data_algo/p18_shortest_path_grid_key_door/
# https://leetcode.com/problems/shortest-path-to-get-all-keys/discuss/364604/Python-Level-by-level-BFS-Solution-(292-ms-beat-97.78)-(similar-problems-listed)

from collections import deque
def get_maze_neighbors(x, y, max_x, max_y):
    adj_list = []
    if x-1 >= 0:
        adj_list.append((x-1, y))
    if x+1 <= max_x-1:
        adj_list.append((x+1, y))
    if y-1 >= 0:
        adj_list.append((x, y-1))
    if y+1 <= max_y-1:
        adj_list.append((x, y+1))
    return adj_list


def find_shortest_path(grid):
    """
    Args:
     grid(list_str)
    Returns:
     list_list_int32
    """
    # Write your code here.
    x_len = len(grid)
    y_len = len(grid[0])
    visited = [[-1]*y_len for i in range(x_len)]
    print("Visited:", visited)
    q = deque()
    result_set = []
    #ancestor_list = []
    #key_map = {}

    def bfs(i, j):
        while q:
            (nodex, nodey), ancestor_list, key_map = q.popleft()
            print("(nodex, nodey), ancestor_list, key_map:", nodex, ",", nodey, " | ", ancestor_list, " | ", key_map)
            #print("ancestor_list id:", id(ancestor_list))
            # Collect previously travelled path
            #print("ancestor_list:", ancestor_list)
            ancestor_list = ",".join([ancestor_list, str(nodex), str(nodey)])
            print("ancestor_list appended:", ancestor_list)
            # Keep track of keys collected
            if grid[nodex][nodey].isalpha() and grid[nodex][nodey].islower():
                print("Found a key:", grid[nodex][nodey])
                key_map[grid[nodex][nodey].upper()] = grid[nodex][nodey]

            if grid[nodex][nodey] == "+":
                # Found target - break and return ancestor list
                print("Found +")
                result_set.append(ancestor_list)
                #return ancestor_list

            for neighborx, neighbory in get_maze_neighbors(nodex, nodey, x_len, y_len):
                valid_neighbor = False
                print("neighborx, neighbory: ", neighborx, ",", neighbory)
                if visited[neighborx][neighbory] != 1 and grid[neighborx][neighbory] != "#":
                    valid_neighbor = True
                    if str(grid[neighborx][neighbory]).isalpha() and grid[neighborx][neighbory].isupper():
                        print("Found alphabet")
                        # Check the key and use it to open the door
                        if grid[neighborx][neighbory] in key_map:
                            print("Found neighbor in key_map")
                            valid_neighbor = True
                            # Since the key is used delete the used key
                            del key_map[grid[neighborx][neighbory]]
                        else:
                            print("Invalid neighbor")
                            valid_neighbor = False

                    if valid_neighbor:
                        q.append(((neighborx, neighbory), ancestor_list, key_map))
                    visited[neighborx][neighbory] = 1

    # Outer loop start from the coordinates matching the start symbol @
    for i in range(x_len):
        for j in range(y_len):
            # No need to check multiple components bcos we know the starting point
            if grid[i][j] == "@":
                print("Starting at: i, j", i, ", ", j)
                visited[i][j] = 1
                q.append(((i, j), "", {}))
                bfs(i, j)
    return result_set

# TODO print(find_shortest_path(["...B", ".b#.", "@#+."]))
# Expected [
# [2, 0],
# [1, 0],
# [1, 1],
# [0, 1],
# [0, 2],
# [0, 3],
# [1, 3],
# [2, 3],
# [2, 2]
# ]
####################################
#https://leetcode.com/problems/critical-connections-in-a-network/solution/
def create_connection_adj_list(number_of_servers, connections):
    adj_list = [[] for _ in range(number_of_servers)]
    for src, dest in connections:
        adj_list[src].append(dest)
        adj_list[dest].append(src)
    return adj_list


def find_critical_connections(number_of_servers, connections):
    """
    Args:
     number_of_servers(int32)
     connections(list_list_int32)
    Returns:
     list_list_int32
    """
    # Create adj list - undirected
    adj_list = create_connection_adj_list(number_of_servers, connections)

    # Create visited and arrival times bookkeeping
    visited = [-1] * number_of_servers
    arrival = [-1] * number_of_servers
    parent = [-1] * number_of_servers
    timestamp = [0]

    # Note: the input is a connected graph, so there are no multiple connected components
    # Check if there is a cycle - if cycle exists there is no critical connection, but if it is not then it is a critical connection
    # use dfs propate through the nodes, update arrival times for each node
    # If upcoming neighbor arrival time of already visited node (except parent) is less than the current node arrival time - it means there is a cycle
    # Inorder to recognize all the nodes in the cycle, each dfs call will recursively return the lowest (smallest) arrival time
    # Finally if the arrival time neighbors is not less than the neighbor, then there is a critical connection - we will capture that node and neighnor info

    result = []

    def dfs(node, prev_node):
        print("Entering node:", node)
        print("Entering prev_node:", prev_node)
        visited[node] = 1
        timestamp[0] = timestamp[0] + 1
        arrival[node] = timestamp[0]

        # For now set this as min_arrival by default on entry for the node
        min_arrival = arrival[node]

        # Get neighbors
        print("Got neighbors:", adj_list[node])
        for neighbor in adj_list[node]:
            # Neighbor not visited yet
            if visited[neighbor] == -1:
                parent[neighbor] = node
                # Find the minimum arrival time returned by the neighbor recursively
                min_arrival_neighbor = dfs(neighbor, node)
                # Compare the neighbor min arrival with min arrival at hand
                min_arrival = min(min_arrival_neighbor, min_arrival)
            else:
                # This is a cycle - check to make sure this no a parent node disguised as a neighbor
                if parent[node] != neighbor:
                    min_arrival = min(arrival[neighbor], min_arrival)

        # If the min arrival time is same, then there is no cycle
        print("min_arrival:", min_arrival)
        print("arrival[node]:", arrival[node])
        print("node:", node)
        print("prev_node:", prev_node)
        if min_arrival == arrival[node] and prev_node is not None:
            print("prev_node:", prev_node)
            print("Appending to result:", [node, prev_node])
            result.append([node, prev_node])
        return min_arrival

    dfs(0, None)

    if len(result) == 0:
        result.append([-1, -1])
    return result
#print(find_critical_connections(5, [[0, 1], [0, 2], [0, 4], [1, 2], [1, 3]]))
#print(find_critical_connections(2, [[0, 1]]))

########################################################
#https://leetcode.com/problems/critical-connections-in-a-network/solution/
def create_crit_connection_adj_list_and_edge_list(number_of_servers, connections):
    adj_list = [[] for _ in range(number_of_servers)]
    uniq_edges = {}
    for src, dest in connections:
        adj_list[src].append(dest)
        adj_list[dest].append(src)
        uniq_edges[(min(src,dest), max(src,dest))] = 1
    return uniq_edges, adj_list

def find_critical_connections_rank_approach(number_of_servers, connections):
    """
    # In this (SLOWER BUT ALTERNATE) approach we will collect all edges first and then eliminate unwanted (not a part of cycle) edges.
    # The remaining edges will be critical connection

    Args:
     number_of_servers(int32)
     connections(list_list_int32)
    Returns:
     list_list_int32
    """
    # Create adj list - undirected
    all_uniq_edges, adj_list = create_crit_connection_adj_list_and_edge_list(number_of_servers, connections)
    visited = [-1] * number_of_servers
    parent = [-1] * number_of_servers
    # No need to collect parents because we can figure that out by using rank-1
    rank = [None] * number_of_servers  # We need to use comparison later, so we will use None as default
    def dfs(node):
        pass
        # TODO
    dfs(0)
    if not all_uniq_edges:
        return [[-1, -1]]
    return list(all_uniq_edges.keys())

#print(find_critical_connections_rank_approach(5, [[0, 1], [0, 2], [0, 4], [1, 2], [1, 3]]))
#print(find_critical_connections_rank_approach(2, [[0, 1]]))
###########################

#https://leetcode.com/problems/critical-connections-in-a-network/solution/

from collections import deque

def get_neighbors(target, possible_words):
    neighbors = []
    if len(possible_words) > 26:
        for c in 'abcdefghijklmnopqrstuvwxyz':
            for i in range(len(target)):
                word = target[:i] + c + target[i + 1:]
                if word in possible_words:
                    neighbors.append(word)
    else:
        for word in possible_words:
            count = 0
            for i in range(len(target)):
                if target[i] != word[i]:
                    count += 1
                if count > 1:
                    break
            if count == 1:
                neighbors.append(word)
    return neighbors

def get_distance(word, stop):
    count = 0
    for i in range(len(word)):
        if word[i] != stop[i]:
            count += 1
        if count > 1:
            return count
    return count

# UNUSED
def is_onechangeaway(src, possible_words):
    #print("possible_words: ", possible_words)
    one_char_change_list = []
    for word in possible_words:
        #print("Src: ", src)
        #print("word: ", word)
        if src == word:
            #print("continue")
            continue
        counter = 0
        for i in range(len(word)):
            if src[i] != word[i]:
                counter += 1
            if counter > 1:
                #print("break")
                break
        if counter == 1:
            one_char_change_list.append(word)
    #print("one_char_change_list:", one_char_change_list)
    return one_char_change_list

# UNUSED
def create_adj_map_one_change_words(words, start, stop):
    adj = {}
    all_words = words + [start, stop]
    for word in all_words:
        adj[word] = is_onechangeaway(word, all_words)
    return adj

def string_transformation(words, start, stop):
    if get_distance(start, stop) == 1:
        return [start, stop]
    #adj_map = create_adj_map_one_change_words(words, start, stop)
    #print(adj_map)
    #result = []
    q = deque([(start, [start])])
    possible_words = set(words + [start, stop])
    visited = {}
    visited[start] = 1

    # To find the shortest path, we will use BFS
    def bfs():
        while q:
            node, prev_nodes = q.popleft()

            """
            # Unused
            if node == stop:
                #print("Reached end string: ", stop)
                if len(prev_nodes) == 0:
                    return [-1]
                return [start] + prev_nodes
            """
            #neighbors = adj_map[node]
            #for neighbor in adj_map[node]:
            for neighbor in get_neighbors(node, possible_words):
                if get_distance(neighbor, stop) == 1:
                    return prev_nodes + [neighbor, stop]

                # Node is not visited
                if neighbor not in visited:
                    visited[neighbor] = 1
                    q.append((neighbor, prev_nodes+[neighbor]))
        return ["-1"]

    # There is no need to find connected components because we know the start point
    result = bfs()
    return result

#print(string_transformation(["cat", "hat", "bad", "had"], "bat", "had"))
#print(string_transformation(["bbb", "bbc"], "bbb", "bbc"))
#print(string_transformation([], "zzzzzz", "zzzzzz"))
#print(string_transformation(["aaa"], "baa", "aab"))
#####################################################################
