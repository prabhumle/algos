import logging
logging.basicConfig(level=logging.DEBUG)


def equal_subset(arr):
    divided_sum = sum(arr)//2
    logging.info("divided_sum: %s", divided_sum)
    dp_table = []  # this will be a 2D bool array default False
    # Create table with all False
    for i in range(len(arr)+1):
        dp_table.append([-1])
        for j in range(1, divided_sum+1):
            dp_table[i].append(-1)

    # Initalize first col for every row
    for i in range(len(arr)+1):
        dp_table[i][0] = True

    # Initalize last row for cols 1-end
    for j in range(1, divided_sum+1):
        logging.info("j:%s", j)
        dp_table[len(arr)][j] = False
    logging.info("dp_table:\n%s", dp_table)

    # Populate DB table
    for i in range(len(arr)-1, -1, -1):
        logging.info("i <---: %s", i)
        for j in range(1, divided_sum+1):
            left = right = False
            logging.info("j ->: %s", j)
            if i+1 < len(arr):
                logging.info("LEFT: i+1: %s|j: %s", i+1, j)
                left = dp_table[i+1][j]
                if j-arr[i] >= 0 and j-arr[i] <= divided_sum:
                    logging.info("RIGHT: i+1: %s|j: %s|arr[i]: %s|j-arr[i]: %s", i+1, j, arr[i], j-arr[i])
                    right = dp_table[i+1][j-arr[i]]
            logging.info("left: %s | right: %s", left, right)
            dp_table[i][j] = left or right

    for i in range(len(dp_table)):
        print(dp_table[i])
    return dp_table[0][divided_sum]

    # Base conditions:


    # Define dp_table dimensions based on the input to the recurrence formula and size of inputs & base cases
    #dp_table[len(arr)+1][len(range(sum))+1]

    # Instantiate dp tables with base values
    #for i in arr[]:
        # set i vals for sum 0
        #DP[i][sum] =
    #for j in range(sum):
        # set j vals
        #DP[i][sum] =

    # Write the iteration logic which uses the initializes value, and populates rest of DP

    # return DP[0][11]


#res = equal_subset([-3, 7, 2, 1, 3, 10])
#res = equal_subset([1,2,3])
res = equal_subset([3, 1, 1, 2, 2, 1])

logging.info("Res: %s", res)
#equal_subset([1, 2, 3])