# Selection sort

# Search for the first smallest item in array and move it to first index, then search for the second smallest move to second index ..

import logging
logging.basicConfig(level=logging.DEBUG)

def selection_sort(input_array):
    input_length = len(input_array)
    i = 0
    while i <= input_length-1:
        logging.info("i: %s", i)
        logging.info("Arr first loop: %s", input_array)
        j = i+1
        while j <= input_length-1:
            logging.info("j: %s", j)
            if input_array[j] < input_array[i]:
                input_array[i], input_array[j] = input_array[j], input_array[i]
            logging.info("Arr second loop: %s", input_array)
            j += 1
        i += 1
    logging.info("Arr final loop: %s", input_array)

input = [5, 3, 1, 4, 2]
selection_sort(input)