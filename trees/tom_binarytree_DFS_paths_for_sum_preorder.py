# The live class was from Tom Gilmour but the logic is from Omkar's PPT
# https://drive.google.com/file/d/1pyDINp9utJNG2uifRRGneRpg5Q5FPolu/view


# Leet#113 Pathsum Given tree and sum, find all root-to-leaf paths that creates the sum

import logging
logging.basicConfig(level=logging.DEBUG)


# The node creation logic is simply based on reference from https://www.geeksforgeeks.org/binary-tree-set-1-introduction/
class TreeNode(object):
    def __init__(self, key, data=None):
        self.key = key
        self.data = data
        self.left = None
        self.right = None


# Create a tree provided in the problem Leet#102
root = TreeNode(5)
root.left = TreeNode(4)
root.right = TreeNode(8)
root.left.left = TreeNode(11)
root.left.left.left = TreeNode(7)
root.left.left.right = TreeNode(2)

# Add children to right child of root
root.right.left = TreeNode(13)
root.right.right = TreeNode(4)
root.right.right.left = TreeNode(5)
root.right.right.right = TreeNode(1)


# Solution
def dfs(node, expected_sum):
    if node is None:
        return []

    paths = []

    def helper(node, expected_sum, current_sum, path):

        # NOTE: THis will be a preorder path because we take action at root before left or right node
        logging.info("Path: %s|current_sum: %s", path, current_sum)
        # If reached the leaf node
        current_sum = current_sum+node.key
        path.append(node.key)
        logging.info("Updated Path: %s|current_sum: %s", path, current_sum)

        if node.left is None and node.right is None:
            logging.info("-----In leafffff --------")
            logging.info("expected_sum: %s == current_sum: %s", expected_sum, current_sum)
            if expected_sum == current_sum:
                logging.info("MATCH !!!!!!!! expected_sum: %s == current_sum: %s", expected_sum, current_sum)
                paths.append(path[:])
                return
            logging.info("paths: %s", paths)
        else:
            if node.left:
                logging.info("Inside LEFT node")
                helper(node.left, expected_sum, current_sum, path)
                logging.info("Popping from LEFT")
                path.pop()
                logging.info("Popped from LEFT path: %s", path)
            if node.right:
                logging.info("Inside RIGHT node")
                helper(node.right, expected_sum, current_sum, path)
                logging.info("Popping from RIGHT")
                path.pop()
                logging.info("Popped from RIGHT path: %s", path)
    helper(node, expected_sum, 0, [])
    logging.info("Result: %s", paths)


dfs(root, 22)

