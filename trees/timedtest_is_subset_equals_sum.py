import logging
logging.basicConfig(level=logging.DEBUG)

# Complete the function below.

def check_if_sum_possible(arr, k):

    def helper(slate, i, k):
        logging.info("Slate: %s | i: %s", slate, i)
        # Base class
        if i == len(arr) or slate >= k:
            logging.info("Returning from base class")
            if slate == k:
                logging.info("Slate sum matches k (%s): %s", k, slate)
                result = 1
            else:
                result = 0
            return result
        else:
            #slate = slate + arr[i]
            # Include
            logging.info("Include Slate: %s | i: %s", slate, i)
            result = helper(slate+arr[i], i+1, k)

            if result != 1:
                # Exclude
                #slate = slate - arr[i]
                logging.info("Exclude Slate: %s | i: %s", slate, i)
                result = helper(slate, i+1, k)

        return result

    result = helper(0, 0, k)
    logging.info("Result: %s", result)
    return result


arr = [1,2, 3, 4,5,6,7]
check_if_sum_possible(arr, 7)