# The live class was from Tom Gilmour but the logic is from Omkar's PPT
# https://drive.google.com/file/d/1pyDINp9utJNG2uifRRGneRpg5Q5FPolu/view

import logging
logging.basicConfig(level=logging.DEBUG)


class NAryNode(object):
    def __init__(self, key, data=None):
        self.key = key
        self.data = data
        self.children = []


root = NAryNode(1)
root.children.append(NAryNode(3))
root.children.append(NAryNode(2))
root.children.append(NAryNode(4))

first_child = root.children[0]
first_child.children.append(NAryNode(5))
first_child.children.append(NAryNode(6))



