# Libraries Included:
# Numpy, Scipy, Scikit, Pandas

"""
m -> number of rows in a grid
n -> number of columns in a grid

return number of possible unique paths from the top left of the grid to the bottom right of the grid

paths consist of moving down or right

m, n >= 1

m = 3
n = 2
s -> y -> y -> e

right, down, down
_______
| S|y  |
|--|--|
| x |y1  |
|--|--|
| x1 | E|
-------
s, x,x1,E
s, x, y1, E
S, y, y1, E


00 01 02
10 11 12
20 21 22


2^(m + n)


pythontutor.com
"""
def uniquePaths(m, n):
    #starting_point = (0, 0)

    counter = [0]
    def dfs(i, j):
        if i > m-1 or j > n-1:
            return

        if i == m-1 and j == n-1:
            counter[0] += 1
        else:
            if j+1 <= n-1:
                dfs(i, j+1)
            if i+1 <= m-1:
                dfs(i+1, j)

    dfs(0, 0)
    return counter[0]

print(uniquePaths(3, 1))

# Feedback from Harry
# Need to stop saying brute force
# Provide solution and suggest a way to optimize it
# Come-up with examples
# Walk-through test inputs
# Practice as many as you can for DP practice
# python indentation practice











