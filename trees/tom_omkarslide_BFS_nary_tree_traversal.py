# The live class was from Tom Gilmour but the logic is from Omkar's PPT
# https://drive.google.com/file/d/1pyDINp9utJNG2uifRRGneRpg5Q5FPolu/view


# Given an n-ary tree return level order traversal  of node values from left to right level by level
import logging
logging.basicConfig(level=logging.DEBUG)
from collections import  deque

class NAryNode(object):
    def __init__(self, key, data=None):
        self.key = key
        self.data = data
        self.children = []


root = NAryNode(1)
root.children.append(NAryNode(3))
root.children.append(NAryNode(2))
root.children.append(NAryNode(4))

# Add more children to first root child
root.children[0].children.append(NAryNode(5))
root.children[0].children.append(NAryNode(6))


def n_ary_tree_traversal(root):
    result = []

    if root is None:
        return result

    # Initialize queue
    q = deque([root])

    while len(q) != 0:
        no_of_items_in_q = len(q)
        logging.info("Number of nodes/length of current queue: %s", no_of_items_in_q)
        same_level_nodes = []
        for _ in range(no_of_items_in_q):
            node = q.popleft()
            same_level_nodes.append(node.key)
            for child in node.children:
                q.append(child)
        result.append(same_level_nodes)
    logging.info("Result: %s", result)


n_ary_tree_traversal(root)

