
# Get grid/matrix of chess n*n
# Start from (start_row, start_col) of knight
# This is a shortest path (undirected - knight can both ways) algo, so we will use BFS - we will find neighbors along the path as the pattern is known
# No outer loop needed because we know the only starting point to begin with

from collections import deque
import logging
logging.basicConfig(level=logging.DEBUG)

def get_neighbours(x, y, grid_max_rows, grid_max_cols):
    grid_max_rows = grid_max_rows-1
    grid_max_cols = grid_max_cols-1

    neighbor_list = []
    if x-1 >= 0 and y-2 >= 0:
        neighbor_list.append((x-1, y-2))
    if x-1 >= 0 and y+2 <= grid_max_cols:
        neighbor_list.append((x-1, y+2))
    if x-2 >= 0 and y-1 >= 0:
        neighbor_list.append((x-2, y-1))
    if x-2 >= 0 and y+1 <= grid_max_cols:
        neighbor_list.append((x-2, y+1))
    if x+1 <= grid_max_rows and y-2 >= 0:
        neighbor_list.append((x+1, y-2))
    if x+1 <= grid_max_rows and y+2 <= grid_max_cols:
        neighbor_list.append((x+1, y+2))
    if x+2 <= grid_max_rows and y-1 >= 0:
        neighbor_list.append((x+2, y-1))
    if x+2 <= grid_max_rows and y+1 <= grid_max_cols:
        neighbor_list.append((x+2, y+1))
    return neighbor_list


def find_minimum_number_of_moves(rows, cols, start_row, start_col, end_row, end_col):
    chess_board = [[-1]*cols for j in range(rows)]
    min_move = [0]

    def bfs(sr, sc, er, ec):
        chess_board[sr][sc] == 1  # Mark as visited
        hop = 0
        q = deque([((sr, sc), hop)])

        logging.info("Starting: %s | (%s, %s)", hop, sr, sc)
        while len(q) > 0:
            logging.info("+++++++++Hop incremented++++++++++++++++++")
            #hop += 1
            (r, c), hop = q.popleft()
            if r == end_row and c == end_col:
                logging.info("Found match at hop: %s - coordinates (%s, %s)", hop, r, c)
                return hop

            logging.info("Processing Neigbrs for: (%s, %s)", r, c)
            logging.info("Neighbours: %s", get_neighbours(r, c, rows, cols))
            for neighbor_x, neighbor_y in get_neighbours(r, c, rows, cols):
                logging.info("Processing Neighbr: (%s, %s)", neighbor_x, neighbor_y)
                if chess_board[neighbor_x][neighbor_y] == -1:
                    logging.info("Adding new neighbor to q")
                    q.append(((neighbor_x, neighbor_y), hop+1))
                    logging.info("Marking new neighbor as visited and increasing hop")
                    chess_board[neighbor_x][neighbor_y] = 1
                    logging.info("Updated unvisited neighbor - hop: %s | (%s, %s)", hop, neighbor_x, neighbor_y)
        return -1
    return bfs(start_row, start_col, end_row, end_col)

result = find_minimum_number_of_moves(5, 5, 0, 0, 4, 1)
logging.info("Result: %s", result)