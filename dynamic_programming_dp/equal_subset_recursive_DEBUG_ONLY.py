import logging
logging.basicConfig(level=logging.DEBUG)

def equal_subset(arr):

    if sum(arr) % 2 != 0:
        return False

    divided_sum = int(sum(arr)/2)
    logging.info("divided_sum: %s", divided_sum)

    path_taken = []

    def helper(i, curr_sum):

        logging.info("i: %s | curr_sum: %s", i, curr_sum)
        if i >= len(arr):
            return False

        #if curr_sum > divided_sum:
        if curr_sum < 0:
            return False


        #if curr_sum == divided_sum:
        if curr_sum == 0:
            return True

        path_taken.append(arr[i])

        logging.info("Include =>>>>>>>>>>> IIIIIIIIIIII")
        logging.info("path_taken: %s", path_taken)
        include = helper(i+1, curr_sum - arr[i])
        logging.info("include: %s", include)
        if include:
            return True

        logging.info("Exclude =>>>>>>>>>>> EEEEEEEEEEEE")
        exclude = helper(i+1, curr_sum)
        logging.info("exclude: %s", exclude)
        logging.info("i+1: %s | include: %s | exclude: %s", i+1, include, exclude)
        if exclude:
            return True

        logging.info("path_taken: %s|popping: %s|arr[i]: %s", path_taken, i, arr[i])
        path_taken.pop(i)
        logging.info("Popped: %s", path_taken)
        return False

    result = helper(0, 11)
    logging.info("result: %s", result)
    logging.info("path_taken: %s", path_taken)

equal_subset([-3, 7, 2, 1, 3, 10])