"""
Given an array and a target number, find the indices of the two values from the array that sum up to the given target number.

Example One:

Input: [5, 3, 10, 45, 1], 6

Output: [0, 4]

Sum of the elements at index 0 and 4 is 6.
"""
def two_sum(numbers, target):
    #  Were (left + right = target), we will find right were right = (target - left)
    lookup = {}
    for left in range(len(numbers)):
        target_diff = target - numbers[left]
        # Check if complement is in lookup and pickup index there
        if target_diff in lookup:
            return [lookup[target_diff], left]
        # Add value to lookup if no match
        if numbers[left] not in lookup:
            lookup[numbers[left]] = left
    return [-1, -1]


#print(two_sum([5, 3, 10, 45, 1], 6))
print(two_sum([-100000, -5, 6, 100000, 5, 9, 10], -10))
#print(two_sum([45, 40], 85))
