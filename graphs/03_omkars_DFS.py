#https://docs.google.com/presentation/d/1-adnRYmnI-0qyffHmCgiNls8XhKc1_kFEMYOAXZ9v7g/edit#slide=id.g54afff569f_0_1061
# Slide 157

# Import bidirectional graph created in the first module
from omkars_graphs_create_adjacency_list import *

visited = [-1] * biddir_graph.n
parent = [-1] * biddir_graph.n

def DFS(node):
    visited[node] = 1
    for neighbour in range(0, biddir_graph.adjList[node]):
        if visited[neighbour] == -1:
            DFS(neighbour)


# Outer loop
# Create a graph for every vertex provided and marking unvisited nodes (Vertexes) with the appropriate counter
for vertex in range(0, biddir_graph.n):
        DFS(vertex)

logging.info("visited: %s", visited)
logging.info("parent: %s", parent)




