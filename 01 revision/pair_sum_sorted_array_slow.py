
'''
    Complete the function below.
    The function accepts an INTEGER ARRAY and an INTEGER as parameters
    and is expected to return an INTEGER ARRAY.
'''

# TODO a better answer is available in the acceptable answers section

import logging
logging.basicConfig(level=logging.DEBUG)

def pair_sum_sorted_array(numbers, target):

    results = []
    # Base conditions
    # Array of size 0 or 1
    if len(numbers) in [0, 1]:
        return [-1, -1]

    i = 0
    j = len(numbers) - 1

    while i < j and j >= 1:
        logging.info("i: %s, j: %s, numbers[i]: %s, numbers[j]: %s", i, j, numbers[i], numbers[j])
        sum_val = numbers[i] + numbers[j]
        logging.info("sum_val: %s", sum_val)

        if sum_val == target:
            results.append([i, j])

        if i+1 == j:
            i = 0
            j -= 1
        else:
            i += 1
        logging.info(results)

    if len(results) == 0:
        results.append([-1, -1])
    logging.info("results[0]: %s", results[0])
    return results[0]

print(pair_sum_sorted_array([1, 2, 3, 4, 5, 10], 7))