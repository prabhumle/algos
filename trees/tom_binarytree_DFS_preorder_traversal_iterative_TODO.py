# The live class was from Tom Gilmour but the logic is from Omkar's PPT
# https://drive.google.com/file/d/1pyDINp9utJNG2uifRRGneRpg5Q5FPolu/view


# Leet# 144 In a binary tree return reorder traversal of its nodes

import logging
logging.basicConfig(level=logging.DEBUG)


# The node creation logic is simply based on reference from https://www.geeksforgeeks.org/binary-tree-set-1-introduction/
class TreeNode(object):
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None


# Create a tree provided in the problem
"""
root = TreeNode(1)
root.right = TreeNode(2)
root.right.left = TreeNode(3)
"""
root = TreeNode(1)
root.left = TreeNode(2)
root.right = TreeNode(3)
root.left.left = TreeNode(4)
root.left.right = TreeNode(5)
root.right.left = TreeNode(6)
root.right.right = TreeNode(7)


# Solution
def dfs(node):
    if node is None:
        return 0

    # We will use a stack to implement this

    logging.info("Result: %s", result)

dfs(root)

