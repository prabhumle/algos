
import heapq

class FreqElem(object):

    def __init__(self, number, freq):
        self.number = number
        self.freq = freq

    def __lt__(self, other):
        return self.freq > other.freq

def find_top_k_frequent_elements(arr, k):
    """
    Args:
     arr(list_int32)
     k(int32)
    Returns:
     list_int32
    """
    # Write your code here.
    heap = []
    heapq.heapify(heap)
    count_lkp = {}
    for i in arr:
        if i in count_lkp:
            count_lkp[i] = count_lkp[i] + 1
        else:
            count_lkp[i] = 1

    print(count_lkp)
    for key, val in count_lkp.items():
        heapq.heappush(heap, FreqElem(key, val))

    results = []
    print("k:", k)
    for l in range(k):
        print("l:", l)
        obj = heapq.heappop(heap)
        print(obj.number)
        results.append(obj.number)
    return results

print(find_top_k_frequent_elements([7, 7, 7, 4, 4, 5, 5, 6, 6, 6, 6], 3))