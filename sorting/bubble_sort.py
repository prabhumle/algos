# Start from right-most index and bubble the lowest value to the front of the array
# The inner most iteration always starts from the right most array

import logging
logging.basicConfig(level=logging.DEBUG)

def bubble_sort(arr):
    n = len(arr)
    for i_outer in range(n):
        logging.info("i_outer: %s", i_outer)
        j = n - 1
        while j >= 1:
            logging.info("j: %s", j)
            if arr[j-1] > arr[j]:
                arr[j], arr[j-1] = arr[j-1], arr[j]
            logging.info("Inside: j loop: %s", arr)
            j -= 1
        logging.info(arr)

input = [1,5,3,2,4,9,8,7]

bubble_sort(input)