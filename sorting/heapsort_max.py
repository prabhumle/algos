# Max heap - maximum num will be at the root node in the binary tree

import heapq
import logging

logging.basicConfig(level=logging.DEBUG)

def heapify_max(arr):
    # heapify by default provides min heap, so reverse it by multiplying input by -1 (this will be reverted when the results are popped)
    for i in range(len(arr)):
        arr[i] = arr[i] * -1
    heapq.heapify(arr)
    logging.info("Heapified arr")
    sorted_list = []
    while len(arr) > 0:
        item = heapq.heappop(arr) * -1  # Multiple by -1 to reverse input for max heap only
        sorted_list.append(item)
        logging.info("%s", item)
    print(sorted_list)
    return sorted_list

heapify_max([5,8,1,2,7,3,4])

