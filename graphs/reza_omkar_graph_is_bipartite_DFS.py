# Leet 785 is graph bipartite
# Given an undirected graph return true if it is bipartite
# Omkar video (but using DFS): 2:48:00 https://uplevel.interviewkickstart.com/resource/helpful-class-video-91245-179-347-0

import logging
logging.basicConfig(level=logging.DEBUG)


def is_bipartate(n, edges):

    # Create adjcency list
    adj_list = [ [] for _ in range(n) ]
    for index, edges_list in enumerate(edges):
        for edge in edges_list:
            adj_list[index].append(edge)
            adj_list[edge].append(index)

    # Create visited, parent and distance 1D attributes for the vertices
    visited = [-1] * n
    parent = [-1] * n
    distance = [-1] * n

    def dfs(node):
        visited[node] = 1
        distance[node] += 1
        for neighbor in adj_list[node]:
            if visited[neighbor] == -1:
                visited[neighbor] = 1
                parent[neighbor] = node
                distance[neighbor] = distance[node] + 1
                if not dfs(neighbor):
                    return False
            else:
                if parent[node] != neighbor:
                    # There is a cycle
                    if distance[neighbor] == distance[node]: # In same level - cross link
                        # There is a going to be an odd edge link b/w these two - then non-bipartate
                        return False
        return True

    # outer loop
    for i in range(n):
        if visited[i] == -1:
            if not dfs(i):
                return False
    return True

edges = [[1, 3], [0, 2], [1, 3], [0,2]]
count_vertices = len(edges)
result = is_bipartate(count_vertices, edges)
logging.info("Bipartate: %s", result)

