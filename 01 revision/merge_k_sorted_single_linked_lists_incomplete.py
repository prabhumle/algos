"""
Merge k Sorted Singly Linked Lists
Given k singly linked lists where each linked list is sorted in ascending order, merge all of them into a single sorted linked list.
Example:
Input: [ [1 -> 3 -> 5], [3 -> 4], [7] ].
Output: 1 -> 3 -> 3 -> 4 -> 5 -> 7
"""
https://leetcode.com/problems/merge-k-sorted-lists/solution/

import logging
logging.basicConfig(level=logging.DEBUG)

class SingleLinkedList:

    def __init__(self, data=0, next=None):
        self.data = data
        self.next = next


# TODO - yet to complete
def merge_k_sorted_linked_list(lists):
    main_head = None
    main_tail = None
    ll_lkp = {}
    for ll in lists:
        logging.info("Looping - ll.data: %s", ll.data)
        next_node = ll.next
        logging.info("next_node: %s", next_node)

        if main_head is None:
            main_head = ll

        while True:

            if ll.data in ll_lkp:
                logging.info("In lookup - appending ll.data: %s to existing node", ll.data)
                orig_node = ll_lkp[ll.data]
                ll.next = orig_node.next
                orig_node.next = ll
            else:
                primary_list = main_head
                while True:
                    if ll.data >= primary_list.data:
                        if  primary_list.next is not None and ll.data < primary_list.next.data:


                ll_lkp[ll.data] = ll

            # Break condition
            if next_node is None:
                logging.info("Reached end: ll.data: %s", ll.data)
                main_tail = ll
                break
            else:
                ll = next_node
    return main_head