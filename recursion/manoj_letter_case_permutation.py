

import logging
logging.basicConfig(level=logging.DEBUG)

arr = "A1b32c"

# Result
#result = ["A1b3", "a1b3", "A1B3", "a1B3"]

result = []

def main_letter_case_permutator(arr):
    helper("", 0)
    logging.info("Result: %s", result)

def helper(slate, i):
    logging.info("Starting helper: i: %s", i)
    logging.info("id slate: %s", id(slate))
    if i == len(arr):
        result.append(slate)
        return slate
    else:
        if arr[i].isdigit():
            logging.info("arr[i]: %s is a digit", arr[i])
            helper(slate+arr[i], i+1)
        else:
            logging.info("Inside else")
            # Convert to upper
            helper(slate + arr[i].upper(), i+1)
            # Don't convert
            helper(slate + arr[i].lower(), i+1)

main_letter_case_permutator(arr)


