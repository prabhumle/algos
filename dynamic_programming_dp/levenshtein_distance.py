import logging
logging.basicConfig(level=logging.DEBUG)


def levenshteinDistance(strWord1, strWord2):

    dp_table = []
    # Fill -1 temporarily
    for i in range(len(strWord1)+1):
        dp_table.append([-1]*(len(strWord2)+1))
    logging.info("dp_table created: %s \n", dp_table )

    # Fill base conditions
    for j in range(len(strWord2)+1):
        logging.info("j: %s", j)
        i = len(strWord1)
        logging.info("i: %s", i)
        dp_table[i][j] = len(strWord2)-j
    logging.info("dp_table last row updated: %s \n", dp_table )

    for i in range(len(strWord1)+1):
        logging.info("i: %s", i)
        logging.info("j: %s", j)
        j = len(strWord2)
        dp_table[i][j] = len(strWord1)-i
    logging.info("dp_table last col updated: %s \n", dp_table )

    # Now fill the remaining table decrementing row and col
    for i in range(len(strWord1)-1, -1, -1):
        for j in range(len(strWord2)-1, -1, -1):
            logging.info("Populating: (i,j): (%s, %s)", i, j)
            if strWord1[i] == strWord2[j]:
                dp_table[i][j] = dp_table[i+1][j+1]
            else:
                dp_table[i][j] = 1 + min(dp_table[i][j+1], dp_table[i+1][j], dp_table[i+1][j+1])  # 1+min(insert, delete, replace)
    logging.info("dp_table updated: %s \n", dp_table )
    return dp_table[0][0]



#strWord1 = "cat"
#strWord2 = "bat"
#strWord1 = "qwe"
#strWord2 = "q"
#strWord1 = "kitten"
#strWord2 = "sitting"
#strWord1 = "rat"
#strWord2 = "caper"
res = levenshteinDistance(strWord1, strWord2)
#logging.info("res: %s", res)
