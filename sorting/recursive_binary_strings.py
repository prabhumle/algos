def binary_strings(n):
    if n == 1:
        return [str(0), str(1)]
    else:
        prev_result = binary_strings(n-1)
        result = []
        for s in prev_result:
            result.append(s+'0')
            result.append(s+'1')
        return result


out = binary_strings(5)
print(len(out))