# Factorial

# fact(5) = 4 * 3 * 2 * 1

def factorial(num):

    def helper(n):
        # Base condition
        if n == 0:
            return 1
        else:
            return n * helper(n-1)

    return helper(num)
#print(factorial(5))

# pow(3,5) = 3 * 3 * 3 * 3 * 3
def pow(num, k):
    if k == 1:
        return num
    else:
        return num * pow(num, k-1)
#print(pow(2, 10))


# fib(6) = 0, 1, 1, 2, 3, 5
def fib(n):
    a = []
    if n in [0, 1]:
        return n
    else:
        return fib(n-1) + fib(n-2)
#print(fib(5))

# climb_stairs(10) = [(2, 2, 2, 2, 2), ( 1, 1, 2, 2, 2, 2)....]
def climb_stairs(n):
    results = []

    def helper(i, slate):

        if i == n:
            return results.append(slate[:])
        elif i > n:
            print("Too long")
        else:
            slate.append(2)
            helper(i+2, slate)
            slate.pop()
            slate.append(1)
            helper(i+1, slate)
            slate.pop()
        print("tmp results:", results)

    helper(0, [])
    return results
#print(climb_stairs(10))

from collections import deque
def towers_of_hanoi():
    needle1 = deque([])
    needle2 = deque([])
    needle3 = deque([])
    for i in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']:
        needle1.append(i)
    pass
    # No finished


def binary_strings(n):
    result = []

    def helper(i, slate):
        print(slate)
        if i == n:
            result.append(slate[:])
        else:
            slate.append(0)
            helper(i+1, slate)
            slate.pop()
            slate.append(1)
            helper(i+1, slate)
            slate.pop()

    helper(0, [])
    print(result)
#binary_strings(3)


def decimal_strings(n):
    results = []

    def helper(i, slate):
        print("slate:", slate)
        if i == n:
            results.append(slate[:])
        else:
            for pick_start_num in range(i, n):
                print("pick_start_num:", pick_start_num)
                slate.append(pick_start_num)
                helper(i+1, slate)
                slate.pop()

    helper(0, [])
    print(results)
#decimal_strings(3)

def permute(nums):
    results = []
    def helper(i, slate):
        print("slate:", slate)
        if i == len(nums):
            results.append(slate[:])
        else:
            for j in range(i, len(nums)):
                nums[j], nums[i] = nums[i], nums[j]
                slate.append(nums[i])
                helper(i+1, slate)
                slate.pop()
                nums[j], nums[i] = nums[i], nums[j]  # put the swapped item back
    helper(0, [])
    print(results)
#permute([1,2,3])

def permute_no_dup(nums):
    results = []
    def helper(i, slate):
        print("slate:", slate)
        if i == len(nums):
            results.append(slate[:])
        else:
            print("resetting hashmap for i:", i)
            hmap = {}
            for j in range(i, len(nums)):
                print("hmap before:", hmap)
                if nums[j] not in hmap:
                    print("-----> i:", i)
                    print("-----> j:", j)
                    hmap[nums[j]] = 1
                    print("hmap:", hmap)
                    nums[j], nums[i] = nums[i], nums[j]
                    slate.append(nums[i])
                    helper(i+1, slate)
                    slate.pop()
                    nums[j], nums[i] = nums[i], nums[j]  # put the swapped item back
    helper(0, [])
    print(results)
#permute_no_dup([1, 1, 2])


def phone_no_letter_combinations(num):
    phone_no_map = {
        2: "abc",
        3: "def",
        4: "ghi",
        5: "jkl",
        6: "mno",
        7: "pqrs",
        8: "tuv",
        9: "wxyz"
    }
    # Convert input num into individual characters:
    nums = str(num)
    print("nums:", nums)
    result = []

    def helper(i, slate):
        if i == len(nums):
            result.append(slate[:])
        else:
            for j in range(len(phone_no_map[int(nums[i])])):
                slate.append(phone_no_map[int(nums[i])][j])
                helper(i+1, slate)
                slate.pop()
        print(slate)

    helper(0, [])
    print("result:", result)
    print("len(result):", len(result))
#phone_no_letter_combinations(234)


def k_num_out_of_n(n, k):
    results = []

    def helper(n, k, i, slate):
        if len(slate) == k:
            results.append(slate[:])
            return  # Don't continue
        if i == n+1:
            return
        slate.append(i)
        helper(n, k, i+1, slate)
        slate.pop()
        helper(n, k, i+1, slate)

    helper(n, k, 1, [])
    print("results:", results)
#k_num_out_of_n(4, 2)


def subsetsum(nums, k):
    results = []
    def helper(i, slate, sum_value):

        if sum_value == k:
            results.append(slate[:])
            return  # Exit/prune if sum found

        if i == len(nums) or sum_value > k:
            return

        slate.append(nums[i])
        helper(i+1, slate, sum_value + nums[i])
        slate.pop()
        helper(i+1, slate, sum_value)

    helper(0, [], 0)
    print("results:", results)
#subsetsum([1,2,3,4,5], 10)


def generate_parantheses(n):
    results = []
    max_items = n*2

    def helper(i, slate, open_p, closed_p):
        print("i:", i)
        print("max_items:", max_items)
        # Back tracking case
        if closed_p > open_p:
            return
        # Base case
        if i == max_items:
            if closed_p == open_p:
                results.append("".join(slate[:]))
            return
        slate.append("(")
        helper(i+1, slate, open_p+1, closed_p)
        slate.pop()
        slate.append(")")
        helper(i+1, slate, open_p, closed_p+1)
        slate.pop()

    helper(0, [], 0, 0)
    print("results:", results)
#generate_parantheses(5)

def palindrome_partitioning(s):

    def is_palindrome(ip):
        print("ip checking palindrome:", ip)
        for ix in range(0, len(ip)):
            if ip[ix] != ip[(len(ip)-1)-ix]:
                return False
        return True

    results = []

    def helper(i, slate):
        print("-------- main i:", i)
        print("slate:", slate)

        if i == len(s):
            results.append(slate[:])

        for pick in range(i, len(s)):
            print("i:", i)
            print("pick:", pick)
            curr_str = "".join(s[i:pick+1])
            #print("curr_str:", curr_str)
            if len(curr_str) > 0 and is_palindrome(curr_str):
                slate.append(curr_str)
                helper(pick+1, slate)
                slate.pop()
    helper(0, [])
    print("results:", results)
#palindrome_partitioning("aab")


def nqueens(n):

    queen_positions = [i for i in range(n)]

    def noconflict(slate):
        print("Checking no conflict:", slate)
        if len(slate) > 1:
            if abs(slate[-1] - slate[-2]) < 2:
                # Check if there are queens nearby
                return False
            if slate[-1] in slate[:-1]:
                # Check if same indexes are already existing
                return False
            for ix in range(len(slate[:-1])):
                # Check diagonal presence for all previous items with new queen inserted
                row_index_diff = abs(ix - (len(slate)-1))
                col_index_diff = abs(slate[ix] - slate[len(slate)-1])
                if row_index_diff == col_index_diff:
                    return False
        return True

    results = []

    def helper(i, slate):
        print("------ main i:", i)
        print("Main slate:", slate)
        if noconflict(slate):
            print("No conflict")
        else:
            print("Conflict!!!!!")
            return

        if i == n:
            buff = []
            for item in slate[:]:
                #buff = []
                str_rep = ''
                for t in range(n):
                    if item == t:
                        str_rep += "Q"
                    else:
                        str_rep += "."
                buff.append(str_rep)
            results.append(buff)
        else:
            for pick in range(i, n):
                print("i and pick:", str(i) + " " + str(pick))
                queen_positions[i], queen_positions[pick] = queen_positions[pick], queen_positions[i]
                slate.append(queen_positions[i])
                helper(i+1, slate)
                queen_positions[i], queen_positions[pick] = queen_positions[pick], queen_positions[i]
                slate.pop()
                print("slate popped")
        print("slate: ", slate)

    helper(0, [])
    print("results:", results)
    print(len(results))
#nqueens(5)


def nqueens_not_fast(n):
    # NOTE!!! This Instructor provided solution is not so fast from PPT (tried in leetcode)
    #https://leetcode.com/problems/n-queens/submissions/

    def noconflict(slate):
        last = len(slate)
        #print("Checking no conflict:", slate)
        if last < 2:
            return True
        else:
            for ix in range(last-1):
                # Check diagonal presence for all previous items with new queen inserted
                row_index_diff = abs(last - 1 - ix)
                col_index_diff = abs(slate[last-1] - slate[ix])
                if row_index_diff == col_index_diff or (slate[ix] == slate[last-1]):
                    return False
        return True

    results = []

    def nqhelper(n, i, slate):
        if i == n:
            buff = []
            for item in slate[:]:
                #buff = []
                str_rep = ''
                for t in range(n):
                    if item == t:
                        str_rep += "q"
                    else:
                        str_rep += "-"
                buff.append(str_rep)
            results.append(buff)
        else:
            for col in range(0, n):
                slate.append(col)
                if noconflict(slate):
                    nqhelper(n, i+1, slate)
                slate.pop()
                #print("slate popped")
        #print("slate: ", slate)

    nqhelper(n, 0, [])
    print("results:", results)
    print(len(results))
    return results
#nqueens_not_fast(5)


def letter_case_permutations(str):
    results = []
    n = len(str)

    def helper(i, slate):
        if i == n:
            print("Reached max len: ", i)
            results.append(''.join(slate[:]))
        else:
            print("Checking: ", str[i])
            if str[i].isdigit():
                slate.append(str[i])
                helper(i+1, slate)
                # No branching
            else:
                # 2 branches - lower & upper
                slate.append(str[i].lower())
                helper(i+1, slate)
                slate.pop()
                slate.append(str[i].upper())
                helper(i+1, slate)
            slate.pop()  # pop for digit or for else upper()
        print(results)
        return results

    return helper(0, [])

#letter_case_permutations("a1z")

def get_permutations(arr):
    # Note: Distinct permutations
    results = []
    n = len(arr)

    def helper(i, slate):
        if i == n:
            results.append(slate[:])
        else:
            print("i - hashmap reset:", i)
            hmap = {}
            for pick in range(i, n):
                if arr[pick] not in hmap:
                    print("hmap:", hmap)
                    print("hmap id:", id(hmap))
                    hmap[arr[pick]] = 1
                    print("hmap updated:", hmap)
                    arr[pick], arr[i] = arr[i], arr[pick]
                    slate.append(arr[i])
                    helper(i+1, slate)
                    slate.pop()
                    arr[pick], arr[i] = arr[i], arr[pick]
                else:
                    print("pick in hashmap:", arr[pick])
        return results
    return helper(0, [])
#print(get_permutations([1,2,2]))


def get_distinct_subsets(str):
    # Does NOT work for dups
    result = []
    sortedstr = sorted(str)
    n = len(str)
    hmap = {}
    def helper(i, slate):
        if i == n:
            subsetstr = ''.join(slate[:])
            if subsetstr not in hmap:
                hmap[subsetstr] = 1
                result.append(subsetstr)
        else:
            slate.append(sortedstr[i])
            helper(i+1, slate)
            slate.pop()
            helper(i+1, slate)
        return result

    return helper(0, [])
#print(get_distinct_subsets("dc"))

def get_distinct_subsets_speed(str):
    results = []
    n = len(str)
    sortedstr = sorted(str)
    def helper(level, slate):

        results.append(''.join(slate[:]))
        if level == n:
            return
        else:
            hmap = {}
            for child in range(level, n):
                if sortedstr[child] not in hmap:
                    hmap[sortedstr[child]] = 1
                    slate.append(sortedstr[child])
                    helper(child+1, slate)
                    slate.pop()
        return results

    return helper(0, [])
#print(get_distinct_subsets_speed("aab"))


num_char_mapping = {
    "1" : [],
    "2": ["a", "b", "c"],
    "3": ["d", "e", "f"],
    "4": ["g", "h", "i"],
    "5": ["j", "k", "l"],
    "6": ["m", "n", "o"],
    "7": ["p", "q", "r", "s"],
    "8": ["t", "u", "v"],
    "9": ["w", "x", "y", "z"],
    "0": []
}

def getWordsFromPhoneNumber(phoneNumber):
    # Write your code here
    results = []
    phoneNumberStr = str(phoneNumber)
    n = len(phoneNumberStr)

    def helper(i, slate):
        print("i:", i)
        if i == n:
            if not len(slate):
                return
            results.append(''.join(slate[:]))
        else:
            if len(num_char_mapping[phoneNumberStr[i]]) == 0:
                helper(i+1, slate)
            else:
                for child in range(0, len(num_char_mapping[phoneNumberStr[i]])):
                    slate.append(num_char_mapping[phoneNumberStr[i]][child])
                    helper(i+1, slate)
                    slate.pop()
        return results

    result = helper(0, [])
    if len(result) == 0:
        return ["-1"]
    return result
#print(getWordsFromPhoneNumber(1010101))


def generate_all_combinations(arr, target):
    # NOTE: Not working for duplicates - check next function
    results = []
    n =len(arr)

    def helper(i, slate, sumval):

        if i == n:
            if sumval == target:
                results.append(slate[:])
            return
        else:
            hmap = {}
            if arr[i] not in hmap:
                hmap[arr[i]] = 1
                slate.append(arr[i])
                helper(i+1, slate, sumval+arr[i])
                slate.pop()
                helper(i+1, slate, sumval)

        return results

    return helper(0, [], 0)

#print(generate_all_combinations([1,1,1,1], 2))


def generate_all_combinations_2(arr, target):

    results = []
    n =len(arr)

    def helper(i, slate, sumval):
        if sumval == target:
            results.append(slate[:])
            return
        if i == n or sumval > target:
            return
        else:
            hmap = {}
            for child in range(i, n):
                if arr[child] not in hmap:
                    hmap[arr[child]] = 1
                    slate.append(arr[child])
                    helper(child+1, slate, sumval+arr[child])
                    slate.pop()

        return results

    return helper(0, [], 0)
#print(generate_all_combinations_2([1,1,1,1], 2))
#print(generate_all_combinations_2([1,2,3], 3))
inp= [42,68,35,1,70,25,79,59,63,65,6,46,82,28,62,92,96,43,28,37,92,5,3,54,93]
r = generate_all_combinations_2(sorted(inp), 83)
print(r)
print(len(r))

