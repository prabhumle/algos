import logging
import collections
logging.basicConfig(level=logging.DEBUG)

nums = [4,8,2,3,5,6,7,8,9]

c = collections.Counter(nums)  # Get number of occurences for each items were item is key
result = []
logging.info("c: %s", c)

for i in range(-10, 10):  # Here we assume that the range of numbers are small -10 to 10
    if i in c:  # From the smallest number to biggest number, we append to a new array based on number of occurrences
        for _ in range(c[i]):
            result.append(i)
print(result)