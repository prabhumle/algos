#Leetcode #22 "Generating Parenthesis"
# similiar leetcode problem is — 560. Subarray Sum Equals K    similiar   to this is - 523. Continuous Subarray Sum

# Given n create a list of paranthesis combination which are balanced
# n = 4
# Example: result = ['()()', '(())']

import logging
logging.basicConfig(level=logging.DEBUG)


def has_non_matching_brackets(available_list, i, n):
    non_matching = False
    if len(available_list) >= 1:
        if available_list[0] == ')':
            non_matching = True
    elif available_list.count(')') > available_list.count('('):
        non_matching = True
    elif i == n:
        if available_list.count('(') == available_list.count(')'):
            non_matching = False
    else:
        non_matching = False
    logging.info('available_list: %s|non_matching: %s', available_list, non_matching)
    return non_matching


def generate_parenthesis(n):
    result = []

    def helper(slate, n, i):
        logging.info("helper - slate: %s| i:%s", slate, i)
        if i == n or has_non_matching_brackets(slate, i, n):
            if i == n:
                result.append(''.join(slate))
            return
        else:
            # Include open paranthesis
            slate.append('(')
            helper(slate, n, i+1)
            slate.pop()
            # Include close paranthesis
            slate.append(')')
            helper(slate, n, i+1)
            slate.pop()

    helper([], n, 0)
    return result


result = generate_parenthesis(4)
logging.info("result:%s", result)

# TODO Fix match condition