# Libraries Included:
# Numpy, Scipy, Scikit, Pandas

"""
m -> number of rows in a grid
n -> number of columns in a grid

return number of possible unique paths from the top left of the grid to the bottom right of the grid

paths consist of moving down or right

m, n >= 1

m = 3
n = 2
s -> y -> y -> e

right, down, down
_______
| S|y  |
|--|--|
| x |y1  |
|--|--|
| x1 | E|
-------
s, x,x1,E
s, x, y1, E
S, y, y1, E


00 01 02
10 11 12
20 21 22


2^(m + n)


pythontutor.com
"""
# This DP solution is post mock interview

# Based on the 3x3  matrix example, considering the starting point 00 - there are two unique paths 01, 10
# (bcos the traversal direction is right or bottom)
# Similarly the # of unique paths for all first row elements and first col elements is 1 - considering only 1 path/direction available
# Now, we can populate the DP values
# Similar/thought example here: https://leetcode.com/problems/unique-paths/solution/

def uniquePaths(m, n):
    # Create dp table
    dp = [[0]*n for _ in range(m)]
    # Setup base condition
    for i in range(n):
        dp[0][i] = 1
    for i in range(m):
        dp[i][0] = 1
    # Populate dp table based on recurrence equation
    for i in range(1, m):
        for j in range(1, n):
            dp[i][j] = dp[i-1][j] + dp[i][j-1]

    return dp[m-1][n-1]

print(uniquePaths(3,2))
print(uniquePaths(3,3))





