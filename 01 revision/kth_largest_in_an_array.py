
'''
   Complete the function below.
   The function takes an INTEGER ARRAY and an INTEGER as inputs and is expected to return an INTEGER.
'''

import heapq

def kth_largest_in_an_array_fastest(numbers, k):
    return heapq.nlargest(k, numbers)[-1]


def kth_largest_in_an_array(numbers, k):
    # Fast
    heap = numbers[0:k]
    print("INitial heap - keep size of heap at k (so size is minimal and time complex O(Nlogk) overall): %s", heap)


    heapq.heapify(heap)

    i = k
    l = len(numbers)

    while i < l:
        print("Pushing: ",  numbers[i])
        heapq.heappushpop(heap, numbers[i])
        i += 1

    return heap[0]

def kth_largest_in_an_array_mine(numbers, k):

    class MaxObj(object):
        def __init__(self, value):
            self.value = value

        def __lt__(self, other):
            return self.value > other.value

    temp = []
    for item in numbers:
        heapq.heappush(temp, MaxObj(item))

    print(temp)
    i = 1
    while i <= k:
        result = heapq.heappop(temp).value
        print("popping: %s", result)
        if i == k:
            print("Result: %s", result)
            return result
        i += 1

print(kth_largest_in_an_array([5, 1, 10, 3, 2], 2))