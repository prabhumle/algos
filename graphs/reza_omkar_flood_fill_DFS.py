# leet 733 Flood fill
# An image is represented by 2D array on integers (0-65535)
# Given a starting pixel (sr, sc) - flood will the image with the new color given

import logging
logging.basicConfig(level=logging.DEBUG)

def get_neighbor_coordinates(x, y, image_matrix):
    # Get (x+1, y), (y+1, y), (x-1, y), (y-1) as long as they are within grid of the image matrix
    neighbor_list = []
    if x+1 <= len(image_matrix)-1:
        neighbor_list.append((x+1, y))
    if y+1 <= len(image_matrix[0])-1:
        neighbor_list.append((x, y+1))
    if x-1 >= 0:
        neighbor_list.append((x-1, y))
    if y-1 >= 0:
        neighbor_list.append((x, y-1))
    return neighbor_list


def flood_fill(x, y, tgt_color, image_matrix):

    # Take the starting position provided & color it with the target color including neighbors using DFS
    def dfs(sr, sc, target_color, actual_color):
        logging.info("Starting at node position: (%s, %s)|color: %s", sr, sc, target_color)
        image_matrix[sr][sc] = target_color
        for x_neighbor, y_neighbor in get_neighbor_coordinates(sr, sc, image_matrix):
            logging.info("Neighbor coordinates: (%s, %s)", x_neighbor, y_neighbor)
            obtained_color = image_matrix[x_neighbor][y_neighbor]
            logging.info("In neighbor: obtained_color: %s| actual_color: %s", obtained_color, actual_color)
            if obtained_color == actual_color:  # Check if it's the color we can to update
                logging.info("No color match")
                image_matrix[x_neighbor][y_neighbor] = target_color
                dfs(x_neighbor, y_neighbor, target_color, actual_color)

    existing_color = image_matrix[x][y]
    logging.info("Existing color: %s|tgt_color: %s", existing_color, tgt_color)
    if existing_color != tgt_color:
        dfs(x, y, tgt_color, existing_color)
    logging.info("Result matrix: %s", image_matrix)


image_matrix = [[1,1,1], [1,1,0], [1,0,1]]
flood_fill(1, 1, 2, image_matrix)