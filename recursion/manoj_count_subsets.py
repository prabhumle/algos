import logging
logging.basicConfig(level=logging.DEBUG)

arr = [5, 3, 2, 4, 1]
# Find the count /number of subsets that sums to 5
# For above example the result is 2 => (4,1) and (2,3)

result = []
counter = 0

# Check if sum of the list of elements collected equals 5, if yes add to result set
# If sum of list elements > 5 stop proceeding further
# if sum of list elements < 5 proceed further by branching to two children until above base conditions are met


def main_count_subsets(arr):
    helper(0, 0, [])
    #logging.info("Result:%s", result)
    logging.info("Counter: %s", counter)


def helper(slate_sum, index, elements_collected):
    logging.info("Starting helper: index: %s | slate: %s | elements_collected: %s", index, slate_sum, elements_collected)
    if slate_sum == 5:
        #result.append(elements_collected)
        global counter
        counter += 1
        return
    elif index == len(arr) or slate_sum > 5:
        # Reached end of array
        return
    else:
        elements_collected.append(arr[index])
        helper(slate_sum+arr[index], index+1, elements_collected)
        helper(slate_sum, index+1, elements_collected)

main_count_subsets(arr)

# TODO fix elements collected