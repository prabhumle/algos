# Complete the function below.

def generate_palindromic_decompositions(s):

    input_len = len(s)
    result = []

    def is_palindrome(slate):
        #print("Check if palindrome:", slate)
        slate_len = len(slate)
        for ix in range(0, slate_len):
            #print("Checking:")
            #print("slate[ix]: slate[(slate_len-1)-ix]:", str(slate[ix]) + ": " +  str(slate[(slate_len-1)-ix]) )
            if slate[ix] != slate[(slate_len-1)-ix]:
                #print("non-palin")
                return False
            #print("palin")
        return True

    def helper(i, slate):
        if i == input_len:
            result.append("|".join(slate[:]))
        else:
            for substr_index in range(i, input_len):
                if is_palindrome(s[i:substr_index+1]):
                    slate.append("".join(s[i:substr_index+1]))
                    helper(substr_index+1, slate)
                    slate.pop()

    helper(0, [])
    #print("result:", result)
    return result

generate_palindromic_decompositions("abracadabra")
