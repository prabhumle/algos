# Given a n queens and nxn chess board, place queens so that they don't conflict each other
# Thinking of a matrix - another queen can't be in the same row or column or diagonal

# Solution - pick 1 queen out of n, and pass the rest of the queens to the subordiante to see if their queen conflicts
# If conflicts the immediate subordinate should place the queen in a non conflicting position

import logging
logging.basicConfig(level=logging.DEBUG)


def no_conflict(prev_queens, curr_row_index, column_index):
    no_conflict = True
    logging.info("in no_conflict fn: Current: (%s, %s)", curr_row_index, column_index)
    for (prev_i, prev_j) in prev_queens:
        logging.info("Prev: (%s, %s)", prev_i, prev_j)
        # Check if in the same column
        if prev_i == curr_row_index:
            no_conflict = False
            break
        # Check if in same row
        elif prev_j == column_index:
            no_conflict = False
            break
        # Check if in same diagonal
        else:
            # if in the same diagonal the difference between i's and j's are 1
            row_diff = abs(curr_row_index-prev_i)
            col_diff = abs(column_index-prev_j)
            logging.info("row_diff, col_diff (%s, %s)", row_diff, col_diff)
            if (row_diff-col_diff) == 0:
                logging.info("Same diagonal")
                no_conflict = False
                break
    logging.info("no_conflict status: %s", no_conflict)
    return no_conflict


def n_queens(n):
    result_positions = []

    def helper(slate_prev_queens, queen_in_row_number, queen_col_pos):
        logging.info("slate_prev_queens: %s | (%s) | col queen: %s", slate_prev_queens, queen_in_row_number, queen_col_pos)

        # Base condition
        # When all rows are looked into exit
        if (queen_in_row_number == n):
            if len(slate_prev_queens) == n:
                intermediate = []
                for i, j in slate_prev_queens:
                    result_template = ["-" for h in range(0, n)]
                    result_template[j] = 'q'
                    intermediate.append(''.join(result_template))
                result_positions.append(intermediate)
                #result_positions.append(slate_prev_queens)
            return
        else:
            # For each position of the queen in the given row, place the queen without conflict
            for queen_col_pos in range(0, n):
                logging.info("QUEEEEEN: %s - Calling helper for the next column choice  %s in the given row", queen_in_row_number, queen_col_pos)
                logging.info("Position: (%s, %s)", queen_in_row_number, queen_col_pos)
                logging.info("slate_prev_queens: %s", slate_prev_queens)
                if no_conflict(slate_prev_queens, queen_in_row_number, queen_col_pos):
                    slate_prev_queens.append((queen_in_row_number, queen_col_pos))
                    helper(slate_prev_queens, queen_in_row_number+1, queen_col_pos)
                    slate_prev_queens = []  # Reset queen for next column position
                else:
                    logging.info("!!!!!!!!!!!!!!!!!!!! Conflict Position: (%s, %s)", queen_in_row_number, queen_col_pos)
                    
    # Call helper func
    helper([], 0, 0)
    logging.info("result_positions: %s", result_positions)

# Call main function
n_queens(7)

# TODO unit test failed for 5, 6, 7 queens