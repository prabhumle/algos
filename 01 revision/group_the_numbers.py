"""
You are given an integer array arr of size n. Group and rearrange them (in-place) into evens and odds in such a way that group of all even integers appears on the left side and group of all odd integers appears on the right side.
Example
Input: [1, 2, 3, 4]
Output: [4, 2, 1, 3]
Order does not matter. Other valid solutions are:
[2, 4, 1, 3]
[2, 4, 3, 1]
[4, 2, 3, 1]
Notes
Input Parameters: There is only one argument: Integer array arr.
Output: Return the same integer array, with evens on left side and odds on the right side.
There is no need to preserve order within odds or within evens.
"""


def solve(arr):
    if len(arr) in [0, 1]:
        return arr

    even = 0
    readp = 0

    while even < len(arr) and readp < len(arr):
        if arr[readp] % 2 == 0:
            arr[even], arr[readp] = arr[readp], arr[even]
            even += 1
        readp += 1
    return arr


result = solve([1, 2, 3, 4, 8, 7, 100, 11, 21, 36])
print(result)


# Complete the function below.
# No need to sort here - we just need to partition the data (right of the bat we an think of merge and quicksort)
# We reject merge sort because of the requirement to use constant aux space - REJECT
# Now, for the partitioning using quick sort - we can pick a random number as a pivot and put all even numbers on the
# left instead of less than
# Array may have duplicates which increases the time complexity of quick sort
# (but that time complexity is with sorting not partitioning which is O(n) but we do that log n times)
# Think of insertion sort - think of a pack of cards check ith item against n-i items and swap the lowest O(n^2)(this does not partition instead focuses on sorting so we discard)
# Brute force - n^2 complexity - Discard
# Insertion sort n^2 complexity -

# A simple partitioning technique were you read the entire array and swap them in place for non-integers till the end of the list seems to be a good solution

def solve(arr):
    if not arr or len(arr) == 1:
        return arr
    left_pointer = 0
    right_pointer = len(arr)-1
    while left_pointer < right_pointer:
        while left_pointer < len(arr) and arr[left_pointer] % 2 == 0:
            left_pointer += 1
        while right_pointer >= 0 and arr[right_pointer] % 2 != 0:
            right_pointer -= 1
        if left_pointer < right_pointer:
            arr[left_pointer], arr[right_pointer] = arr[right_pointer], arr[left_pointer]
    return arr