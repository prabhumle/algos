"""
Zombie Clusters
There are zombies in Seattle. Some of them know each other directly. Others might know each other transitively, through others. For example, if zombies A<->B and B<->C know each other directly, then A and C know each other indirectly and all three belong to one cluster.
Knowing which zombies know each other directly, find the number of the zombie clusters.
Input is a square matrix where each cell, zombies[A][B], indicates whether zombie A knows zombie B directly.
"""
import logging
logging.basicConfig(level=logging.DEBUG)

def zombieCluster(zombies):

    def get_neighbours(x, y, x_length, y_length):
        neighbor_list = []
        x_length -= 1
        y_length -= 1
        if x-1 >= 0:
            neighbor_list.append((x-1, y))
        if y-1 >= 0:
            neighbor_list.append((x, y-1))
        if x+1 <= x_length:
            neighbor_list.append((x+1, y))
        if y+1 <= y_length:
            neighbor_list.append((x, y+1))
        logging.info("neighbor_list: %s", neighbor_list)
        return neighbor_list

    def dfs(xaxis, yaxis, xaxislen, yaxislen):
        zombies_new[xaxis][yaxis] = -1
        for x_neighbor, y_neighbor in get_neighbours(xaxis, yaxis, xaxislen, yaxislen):
            #if zombies_new[x_neighbor][y_neighbor] != -1 and zombies_new[x_neighbor][y_neighbor] != 0:
            if zombies_new[x_neighbor][y_neighbor] == 1:
                zombies_new[x_neighbor][y_neighbor] = -1
                dfs(x_neighbor, y_neighbor, xaxislen, yaxislen)

    # Outerloop
    cluster_id = 0
    logging.info("Input zombies: %s", zombies)
    zombies_new = [[int(e) for e in i] for i in zombies]
    logging.info("Input zombies_new: %s", zombies_new)

    for x_axis in range(len(zombies_new)):
        for y_axis in range(len(zombies_new[0])):
            #if zombies_new[x_axis][y_axis] != -1 and zombies_new[x_axis][y_axis] != 0:
            logging.info("Checking zombies_new[x_axis][y_axis]:%s | x_axis, y_axis: (%s, %s)", zombies_new[x_axis][y_axis], x_axis, y_axis)
            if zombies_new[x_axis][y_axis] == 1:
                logging.info("Starting x_axis, y_axis: (%s, %s)", x_axis, y_axis)
                cluster_id += 1
                logging.info("Launching dfs for cluster_id: %s", cluster_id)
                dfs(x_axis, y_axis, len(zombies_new), len(zombies_new[0]))
                logging.info("zombies_new: %s", zombies_new)
    return cluster_id


#zombies = ["1100", "1110", "0110", "0001"]
zombies = ["10000", "01000", "00100", "00010", "00001"]

clusters = zombieCluster(zombies)
logging.info("Clusters: %s", clusters)