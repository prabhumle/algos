# The live class was from Tom Gilmour but the logic is from Omkar's PPT
# https://drive.google.com/file/d/1pyDINp9utJNG2uifRRGneRpg5Q5FPolu/view


# Leet# 145 In a binary tree return post-order traversal of its nodes

import logging
logging.basicConfig(level=logging.DEBUG)


# The node creation logic is simply based on reference from https://www.geeksforgeeks.org/binary-tree-set-1-introduction/
class TreeNode(object):
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None


# Create a tree provided in the problem
"""
root = TreeNode(1)
root.left = TreeNode(2)
root.right = TreeNode(3)
root.left.left = TreeNode(4)
root.left.right = TreeNode(5)
root.right.left = TreeNode(6)
root.right.right = TreeNode(7)
"""
root = TreeNode(1)
root.left = TreeNode(2)
root.right = TreeNode(3)
root.left.left = TreeNode(4)
root.left.right = TreeNode(5)
root.left.right.left = TreeNode(8)
root.left.right.right = TreeNode(9)
root.right.left = TreeNode(6)
root.right.right = TreeNode(7)


# Solution
def dfs(root):
    # We willl create a stack using a list
    stack = [(root, None)]
    result = []

    while len(stack) != 0:
        node, zone = stack[-1]
        logging.info("Current stack: %s", stack)
        # Check the top most item available in stack

    logging.info("result: %s", result)



dfs(root)

