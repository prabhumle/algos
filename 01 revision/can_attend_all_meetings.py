'''
Complete the function below.
The function takes a 2D INTEGER ARRAY as input and is expected to return an INTEGER.
'''

def can_attend_all_meetings_ORIG(intervals):
    """Solution assuming the input list is NOT IN SORTED ORDER"""
    # We can do a hashmap of all start:end times
    # We can sort by start times O(nlog n)
    # Then we can compare each startime and the previoous end time for conflicts
    endtime_lookup = {}
    start_time_arr = []

    for i, j in intervals:
        if i in endtime_lookup:
            return 0  # There two meetings with the same start time
        endtime_lookup[i] = j
        start_time_arr.append(i)

    sorted_start_time = sorted(start_time_arr)

    i = 0
    while i < len(sorted_start_time)-1:
        if endtime_lookup[sorted_start_time[i]] -  sorted_start_time[i+1] <= 0:
            pass
        else:
            return 0
        i += 1
    return 1


def can_attend_all_meetings(intervals):

    g_start = 0
    g_end = 0
    for x in range(len(intervals)):
        start = intervals[x][0]
        end = intervals[x][1]

        if start < g_end and end > g_start:
            return 0
        else:
            g_end = end
            g_start = start

    return 1

print(can_attend_all_meetings([[0, 10], [5,20], [1, 1000]]))