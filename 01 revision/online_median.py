"""
Complete the function below.
The function accepts an INTEGER_ARRAY as parameter and expected to return an INTEGER_ARRAY.
"""

import heapq

def online_median(stream):

    first_heap = []
    second_heap = []

    results = []

    heapq.heapify(first_heap)
    heapq.heapify(second_heap)

    item_counter = 0

    for item in stream:
        item_counter += 1
        print("-----------------> item: ", item)
        # Check which first heap to insert if the heap is not already empty
        if len(first_heap) > 0 and item > (first_heap[0] * -1):
            print("Pushing to second heap - item > first item of heap:", (first_heap[0] * -1))
            heapq.heappush(second_heap, item)
            print("second_heap: ", second_heap)
        else:
            heapq.heappush(first_heap, item * -1)
            print("Inserted to first_heap: ", first_heap)

        # Balance heap such that the first heap can be equal to second heap or bigger by 1 element/item
        while (len(first_heap)-len(second_heap)) > 1:
            print("1st balancing - len(first_heap):", len(first_heap))
            print("1st balancing - len(second_heap):", len(second_heap))
            popped_first_heap_item = heapq.heappop(first_heap) * -1
            heapq.heappush(second_heap, popped_first_heap_item)
            print("first_heap:", first_heap)
            print("second_heap:", second_heap)

        while (len(second_heap)-len(first_heap)) > 0:
            print("2nd balancing - len(second_heap):", len(second_heap))
            print("2nd balancing - len(first_heap):", len(first_heap))
            popped_second_heap_item = heapq.heappop(second_heap)
            heapq.heappush(first_heap, popped_second_heap_item * -1)
            print("first_heap:", first_heap)
            print("second_heap:", second_heap)
        if item_counter % 2 == 0:
            even_median = ((first_heap[0] * -1) + second_heap[0])//2
            print("even_median:", even_median)
            results.append(even_median)
        else:
            odd_median = (first_heap[0] * -1)
            print("odd_median:", odd_median)
            results.append(odd_median)
        print("tmp results:", results)

    print(results)
    return results


def online_median_slow(stream):

    heap = []
    heapq.heapify(heap)
    results = []

    for item in stream:
        print("pushing into heap:", item)
        heapq.heappush(heap, item)
        current_heap_len = len(heap)
        print("current_heap_len:", current_heap_len)
        cache = []
        if current_heap_len % 2 == 0:
            # Even
            median_index = current_heap_len//2
            print("Even median index:", median_index)
            print(heap)
            while len(heap) > (current_heap_len - (median_index-1)):
                print("Inside even while")
                cache.append(heapq.heappop(heap))
                print("Even heap len:", len(heap))
            median_first = heapq.heappop(heap)
            cache.append(median_first)
            print("median_first:", median_first)
            median_second = heapq.heappop(heap)
            cache.append(median_second)
            print("median_second:", median_second)
            median_for_even = (median_first + median_second)//2
            print("median_for_even:", median_for_even)
            results.append(median_for_even)
            print(heap)
            print(median_for_even)
        else:
            # odd
            median_index = current_heap_len//2
            print("Odd median index:", median_index)
            while len(heap) > (current_heap_len - median_index):
                print("Inside odd while")
                cache.append(heapq.heappop(heap))
                print("odd heap len:", len(heap))
            median_for_odd = heapq.heappop(heap)
            cache.append(median_for_odd)
            print(heap)
            print(median_for_odd)
            results.append(median_for_odd)

        for i in range(len(cache)):
            print("Pushing cache")
            heapq.heappush(heap, cache[i])
        print("heap post if else:", heap)
    print(results)
    return results



online_median([3, 8, 5, 2])
#online_median([1, 2, 3, 4, 5])