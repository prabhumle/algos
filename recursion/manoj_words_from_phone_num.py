"""
# https://uplevel.interviewkickstart.com/resource/rc-codingproblem-56529-277243-244-1535
# https://leetcode.com/problems/letter-combinations-of-a-phone-number/
# Given a seven-digit phone number, return all the character combinations that can be generated according to the following mapping:
# Return the combinations in the lexicographical order.
Example

Input: “1234567”

Output: [

“adgjmp”,

“adgjmq”,

“adgjmr”,

“adgjms”,

“adgjnp”,

...

“cfilns”,

“cfilop”,

“cfiloq”,

“cfilor”,

“cfilos”]

First string “adgjmp” in the first line comes from the first characters mapped to digits 2,3,4,5,6 and 7 respectively. Since digit 1 maps to nothing, nothing is appended before 'a'. Similarly, the fifth string “adgjnp” generated from first characters of 2,3,4,5, second character of 6 and first character of 7. All combinations generated in such a way must be returned in the lexicographical order.

Notes

Input Parameters: The function has one parameter, a string of exactly seven digit characters.

Output: Return an array of the generated string combinations in the lexicographical order. If nothing can be generated, return an array with one string "-1".

Constraints:

Input string is 7 characters long; each character is a digit.
Digits 0 and 1 map to nothing. Other digits map to either three or four different characters each.

Custom Input

Input Format: One line containing the string phoneNumber.

Output Format: Each line represents one generated combination. If nothing can be generated, push only one element "-1" into the resultant array.
"""


import logging
logging.basicConfig(level=logging.DEBUG)

number_char_mapping = {
    0: [],
    1: [],
    2: ['a', 'b', 'c'],
    3: ['d', 'e', 'f'],
    4: ['g', 'h', 'i'],
    5: ['j', 'k', 'l'],
    6: ['m', 'n', 'o'],
    7: ['p', 'q', 'r', 's'],
    8: ['t', 'u', 'v'],
    9: ['w', 'x', 'y', 'z']
}


def words_from_phone_num(phone_num):
    n = len(phone_num)
    phone_nums = [int(i) for i in phone_num]
    logging.info(phone_nums)

    result = []

    def helper(phone_no, slate, index):
        logging.info("MAIN index: %s", index)

        # Base case
        if index == len(phone_no):
            final_slate = ''.join(slate)
            if final_slate == '':
                final_slate = -1
            result.append(final_slate)
            logging.info("RESULT: %s", result)
            return result
        else:
            dialed_num = phone_no[index]

            # If dialed number is 0 or 1 leave the slate as is and proceed
            if dialed_num in [0, 1]:
                helper(phone_no, slate, index+1)

            num_char_list = number_char_mapping[dialed_num]
            for j_char in num_char_list:
                logging.info("index: %s | Dialed num: %s | num_char_list: %s", index, dialed_num, num_char_list)
                logging.info("slate: %s", slate)
                logging.info("j_char: %s", j_char)
                slate.append(j_char)
                helper(phone_no, slate, index+1)
                #slate = []
                slate.pop()

    helper(phone_nums, [], 0)
    logging.info("Result: %s", result)
    return result


words_from_phone_num('1234567')
#words_from_phone_num('01')
