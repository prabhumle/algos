def dutch_flag_sort(balls):
    r, p = 0, 0
    b = len(balls)-1

    while p <= b:
        if balls[p] == "R":
            balls[r], balls[p] = balls[p], balls[r]
            r += 1
            p += 1
        if balls[p] == "G":
            p += 1
        if balls[p] == "B":
            balls[b], balls[p] = balls[p], balls[b]
            b -= 1


def dutch_flag_sort(balls):
    end = len(balls) - 1
    ptr = 0
    r_ptr = 0
    b_ptr = end
    while ptr <= b_ptr:
        if balls[ptr] == "R":
            balls[r_ptr], balls[ptr] = balls[ptr], balls[r_ptr]
            ptr += 1
            r_ptr += 1
        elif balls[ptr] == "G":
            ptr += 1
        else:
            # Blue
            balls[b_ptr], balls[ptr] = balls[ptr], balls[b_ptr]
            #ptr += 1
            b_ptr -= 1

