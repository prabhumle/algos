
from queue import PriorityQueue

q = PriorityQueue()

# insert into queue
q.put((2, 'g'))
q.put((3, 'e'))
q.put((4, 'k'))
q.put((5, 's'))
q.put((1, 'e'))

# remove and return
# lowest priority item
print(q.get())
print(q.get())


class SinglyLinkedListNode:

    def __init__(self, data=0, next=None):
        self.data = data
        self.next = next

q.put((0, SinglyLinkedListNode(0)))
print(q.get())