# The live class was from Tom Gilmour but the logic is from Omkar's PPT
# https://drive.google.com/file/d/1pyDINp9utJNG2uifRRGneRpg5Q5FPolu/view


# Leet# 105 Given preorder and postorder lists - construct a binary tree

import logging
logging.basicConfig(level=logging.DEBUG)

# The node creation logic is simply based on reference from https://www.geeksforgeeks.org/binary-tree-set-1-introduction/
class TreeNode(object):
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None

preorder = [3, 9, 20, 15, 7]
inorder = [9, 3, 15, 20, 7]

def buildBinaryTree(preorder, inorder):

    # Scan through the inorder and create a hashmap because we have to lookup this value
    io_hashmap = {}
    for index, elem in enumerate(inorder):
        io_hashmap[index] = elem
    logging.info("Created Hashmap for inorder: %s", io_hashmap)

    def helper():
        # If empty list is passed return None
        if <>:
            return None
        # If single item list is passed return a leaf node
        if <>:
            return TreeNode()

        # Recursive class - To construct the tree
        rootnode = TreeNode(preorder[left_preo_root])

        # Get length of left and right subtree from io
        index_of_root_in_io = hashmap[preorder[left_preo_root]]
        length_of_left_io =  index_of_root_in_io - left_io_start
        length_of_right_io = right_io_end - index_of_root_in_io

        left_preo_start = left_preo_root + 1
        left_preo_end = left_preo_start + length_of_left_io
        right_preo_start = left_preo_end + 1
        left_preo_end = right_preo_start + length_of_right_io

        left_io_end = index_of_root_in_io-1
        right_io_start = index_of_root_in_io+1
        right_io_end =

        rootnode.left = helper(preorder, inorder, start_preo, end_preo, start_ino, end_io)


        return rootnode
    binarytree = helper()

buildBinaryTree(preorder, inorder)
