import logging
logging.basicConfig(level=logging.DEBUG)


def  levenshteinDistance(strWord1, strWord2):

    nw1 = len(strWord1)
    nw2 = len(strWord2)

    target_word = strWord2

    def helper(src_p, tgt_p):
        logging.info("src_p: %s | strWord1[src_p:]: %s", src_p, strWord1[src_p:])
        logging.info("tgt_p: %s | strWord2[tgt_p:]: %s", tgt_p, strWord2[tgt_p:])

        if len(strWord1[src_p:]) == 0:
            logging.info("strWord1 remaining blank")
            return len(strWord2[tgt_p:])

        if len(strWord2[tgt_p:]) == 0:
            logging.info("strWord2 remaining blank")
            return len(strWord1[src_p:])

        if strWord1[src_p] == strWord2[tgt_p]:
            logging.info("Matching chars returning proceed to next iteration")
            return helper(src_p+1, tgt_p+1)

        insert = helper(src_p, tgt_p+1)
        delete = helper(src_p+1, tgt_p)
        replace = helper(src_p+1, tgt_p+1)

        logging.info("insert: %s", insert)
        logging.info("delete: %s", delete)
        logging.info("replace: %s", replace)
        min_val = min(insert, delete, replace) + 1
        logging.info("min_val: %s", min_val)
        return min_val

    steps = helper(0, 0)
    logging.info("Steps: %s", steps)


#strWord1 = "cat"
#strWord2 = "bat"
#strWord1 = "qwe"
#strWord2 = "q"
#strWord1 = "kitten"
#strWord2 = "sitting"
strWord1 = "rat"
strWord2 = "caper"
levenshteinDistance(strWord1, strWord2)