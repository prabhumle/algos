import logging
logging.basicConfig(level=logging.DEBUG)
# Complete the countWaysToClimb function below.
"""
Count Ways To Reach The Nth Step
There is a staircase with n steps. A person standing at the 0-th step wants to reach the n-th one. They are capable of jumping up by certain numbers of steps at a time.
Given how the person can jump, count the number of ways they can reach the top.
"""

def countWaysToClimb(steps, n):

    if len(steps) == 0 or n == 0:
        return 0

    def helper(i):
        """
        :param i: Step currently on
        :param sum: Total steps climbed
        :return: Number of different ways to clumb
        """

        if i == n:
            logging.info("Matching i: %s, n: %s - Returning 1", i, n)
            return 1

        if i > n:
            logging.info(" i (%s) > n (%s) - returning 0", i, n)
            return 0

        total_ways = 0
        for step in steps:
            logging.info("i: %s |steps taken: %s| will move to step (i+step) %s", i, step, i+step)
            way = helper(i+step)
            total_ways += way
        logging.info("total_ways: %s", total_ways)
        return total_ways

    out = helper(0)
    return out


#result = countWaysToClimb([1,2], 2)
#result = countWaysToClimb([2, 3], 7)
result = countWaysToClimb([1,2], 1)
logging.info("result: %s", result)