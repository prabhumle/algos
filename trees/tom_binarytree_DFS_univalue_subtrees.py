# The live class was from Tom Gilmour but the logic is from Omkar's PPT
# https://drive.google.com/file/d/1pyDINp9utJNG2uifRRGneRpg5Q5FPolu/view


# Leet# 250 In a binary tree count the number of univalued subtrees

import logging
logging.basicConfig(level=logging.DEBUG)


# The node creation logic is simply based on reference from https://www.geeksforgeeks.org/binary-tree-set-1-introduction/
class TreeNode(object):
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None


# Create a tree provided in the problem Leet#102
root = TreeNode(5)
root.left = TreeNode(1)
root.right = TreeNode(5)
root.left.left = TreeNode(5)
root.left.right = TreeNode(5)
root.right.right = TreeNode(5)
#root.right.right.right = TreeNode(5)


# Solution
def dfs(node):
    if node is None:
        return 0

    no_of_univalue_subtrees = [0]

    def helper(node):
        isunival = True
        if node.left is None and node.right is None:
            # All leaf nodes are univalue subtrees
            no_of_univalue_subtrees[0] += 1
            #return True
        else:
            isunival_l = True
            isunival_r = True
            parent_node_val = node.val
            if node.left:
                isleftnode_unival = helper(node.left)
                if not isleftnode_unival or parent_node_val != node.left.val:
                    isunival_l = False
            if node.right:
                isrightnode_unival = helper(node.right)
                if not isrightnode_unival or parent_node_val != node.right.val:
                    isunival_r = False

            if not isunival_r or not isunival_l:
                isunival = False
            else:
                no_of_univalue_subtrees[0] += 1

        return isunival

    helper(node)
    logging.info("unival subtrees: %s", no_of_univalue_subtrees[0])

dfs(root)

