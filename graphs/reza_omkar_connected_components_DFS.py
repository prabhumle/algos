# LEET 323 - No of connected compoents in an undirected grapth
#https://docs.google.com/presentation/d/1-adnRYmnI-0qyffHmCgiNls8XhKc1_kFEMYOAXZ9v7g/edit#slide=id.g6366b029a6_0_4

# Given n number of nodes and undirected edges find number of connected components

import logging
logging.basicConfig(level=logging.DEBUG)


def get_adj_list(n, edges):
    adj_list = [[] for _ in range(n)]
    logging.info("adj_list: %s", adj_list)
    for source, target in edges:
        adj_list[source].append(target)
        # The graph is bidirectional so update the other way too
        adj_list[target].append(source)
    return adj_list


def main(n, edges):

    def dfs(node, comp):
        visited[node] = comp
        for neighbour in adj_list[node]:
            if visited[neighbour] == -1:
                parent[neighbour] = node
                dfs(neighbour, comp)

    # Prepare adjacency list
    adj_list = get_adj_list(n, edges)  # Takes m+n time

    # For each node at a time - check if they are part of the graph or not by assigning a new id for each visit from the given nodes
    # Let us use DFS because the code is much easier to read
    visited = [-1] * n
    parent = [-1] * n
    component = 0
    for vertex in range(n):
        if visited[vertex] == -1:
            component += 1
            dfs(vertex, component)
    return component


#result = main(5, [[0, 1], [1,2], [3, 4]])
result = main(5, [[0, 1], [1,2], [2, 3], [3, 4]])
logging.info("Connected components: %s", result)

# Time complexity => Adj list creation: O(2m+n) + DFS (for each vertex we go through the adj list 1 time O(m+n)?
# Space complexity => Explicit=> visited + parent: O(n+n) + Adj List O(m+n) + Implicit Stack Space O(m)


