
'''
    Complete the function below.
    The function accepts an INTEGER ARRAY and an INTEGER as parameters
    and is expected to return an INTEGER ARRAY.
'''

import logging
logging.basicConfig(level=logging.DEBUG)

def pair_sum_sorted_array(numbers, target):
    results = []
    left = 0
    right = len(numbers) - 1

    # The array is already sorted
    while left < right:
        sum_val = numbers[left] + numbers[right]
        if sum_val == target:
            return [left, right]
            """
            # optional to find all combinations
            # results.append([left, right])
            # right -= 1  
            """
        elif sum_val < target:
            left += 1
        else:
            # sum_val > target
            right -= 1
    if len(results) == 0:
        return [-1, -1]
    return results

print(pair_sum_sorted_array([1, 2, 3, 4, 5, 10], 7))