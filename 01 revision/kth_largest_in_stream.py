#
# The function accepts an INTEGER and two INTEGER_ARRAYS as parameters and
# is expected to return an INTEGER_ARRAY.
#

import heapq
def kth_largest(k, initial_stream, append_stream):
    """https://leetcode.com/problems/kth-largest-element-in-a-stream/solution/
    """
    results = []
    heapq.heapify(initial_stream)

    i = 0
    while i < len(append_stream):
        heapq.heappush(initial_stream, append_stream[i])

        while len(initial_stream) > k:
            heapq.heappop(initial_stream)
        results.append(initial_stream[0])
        i += 1
    return results

def kth_largest_slow(k, initial_stream, append_stream):
    # Write your code here

    # We will use merge sort merging & sorting

    result = []
    for a_idx in range(len(append_stream)):
        print("Appending:", append_stream[a_idx])
        initial_stream.append(append_stream[a_idx])
        i_len = len(initial_stream)

        i = 0
        while i <= i_len-2:
            print("i:", i)
            print("initial_stream (with new elem): ", initial_stream)
            # Check the new appended integer
            if initial_stream[i] < initial_stream[-1]:
                pass
            else:
                bkp_new = initial_stream[-1]
                print("initial_stream[i+1:] ", initial_stream[i+1:])
                print("initial_stream[i:] ", initial_stream[i:])
                initial_stream[i+1:] = initial_stream[i:-1]
                initial_stream[i] = bkp_new
                break
            print("new initial_stream:", initial_stream)
            print("increment i")
            i += 1
            print("incremented i", i)
        # Get kth largest item
        print("kth largest:", initial_stream[-1*k])
        result.append(initial_stream[-1*k])
    return result

print(kth_largest(2, [2,1], [1, 1, 1, 1]))