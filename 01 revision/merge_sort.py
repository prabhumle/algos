"""
Given a list of numbers, your task is to sort it using Merge Sort algorithm.
"""

import logging
logging.basicConfig(level=logging.INFO)


def helper(arr, start, end):
    logging.info("start: %s | end: %s", start, end)

    # Base condition - What does the leaf level worker do
    if start == end:
        return

    # What does middle level worker do - divide the work and share to subordinates
    mid = int((start+end)/2)
    helper(arr, start,  mid)
    helper(arr, mid+1, end)

    # Merge two arrays
    logging.info("XXXX RESetting AUX")
    aux = []
    i = start
    j = mid+1
    while i <= mid and j <= end:
        if arr[i] <= arr[j]:
            aux.append(arr[i])
            i += 1
        else:
            aux.append(arr[j])
            j += 1
    while i <= mid:
        logging.info("i: %s mid: %s", i, mid)
        aux.append(arr[i])
        i += 1
    while j <= end:
        logging.info("j: %s end: %s", j, end)
        aux.append(arr[j])
        j += 1

    logging.info("Setting aux: %s value to arr", aux)
    arr[start:end+1] = aux[:]
    return


def merge_sort(arr):
    helper(arr, 0, len(arr)-1)
    return arr

res = merge_sort([6, 2, 1, 4, 3, 5])
print(res)