def how_many_BSTs(n):

    # For each root node in the sorted list, find the num of left subtrees and right subtrees
    # Total number of combination of trees by multiplying left and right subtrees
    # Base conditions:  (if len(input) = 0 or len(input) = 1) then num_of_trees=1

    # This was challenging to work with the slate concept. So, we will just use the counter logic
    # Using approach suggested here: https://leetcode.com/problems/unique-binary-search-trees/solution/
    d = {}

    def helper(l, r):
        if r <= l:
            return 1
        if (l, r) in d:
            return d[(l, r)]
        sum_val = 0
        for middle in range(l, r+1):
            left = helper(l, middle-1)
            right = helper(middle+1, r)
            sum_val += left * right
        d[(l, r)] = sum_val
        return sum_val
    return helper(0, n-1)

print(how_many_BSTs(2))