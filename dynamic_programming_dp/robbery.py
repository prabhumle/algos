"""
There are n houses built in a line, each of which contains some value in it. A thief is going to steal the maximal value in these houses, but he cannot steal in two adjacent houses because the owner of a stolen house will tell his two neighbors on the left and right side. What is the maximal stolen value?
For example, if there are four houses with values [6, 1, 2, 7], the maximal stolen value is 13, when the first and fourth houses are stolen.
"""
####
# FIRST - Try greedy approach
# This is not working becuase there are different/too many combinations resulting in different values

######################################
# Step1 - Recurrence formula:
####
# First I construct a tree with exclude/include choices for each i (Starting with 0)
# Second The main goal is return the sum of values of each robbed house - so I will add and return the value whenever "include" choice is made
# The return value for each child must be to return the sum of values robbed

# Base conditions:
#  - Rob house if there is only one house
#  - No robbing if 0 houses
#
####################################
# Step -2:
###
# Determine DP table (size, datastructure)
# Initiate DP table
# Traversal direction of DP table
# Populate DP table
########################################
import logging
logging.basicConfig(level=logging.DEBUG)

def get_max_robbed(values):

    def dfs(i, curr_summed_val):
        logging.info("i:%s| curr_summed_val:%s", i, curr_summed_val)
        if i >= len(values):
            return curr_summed_val
        #if len(values[i:]) == 1:
        #    return values[i]
        # Recursion logic
        logging.info("IIIIIIIIIIIIIIIIIIIIIIIInclude value")
        include = dfs(i+1, curr_summed_val+values[i])
        logging.info("include: %s", include)
        logging.info("EEEEEEEEEEEEEEEEEEEEEEEEEEexclude value")
        exclude = dfs(i+1, curr_summed_val)
        logging.info("Exclude: %s", exclude)
        max_val = max(include, exclude)
        logging.info("max_val: %s", max_val)
        return max_val

    out = dfs(0, 0)
    return out

in_vals = [6, 2, 1, 9]
results = get_max_robbed(in_vals)
logging.info("Results: %s", results)

