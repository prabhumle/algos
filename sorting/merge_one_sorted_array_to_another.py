import logging
logging.basicConfig(level=logging.DEBUG)
logging.debug('This will get logged')

def merger_first_into_second(arr1, arr2):
	n1 = len(arr1)
	logging.info("Len arr1: %s", n1)
	n2 = len(arr2)
	logging.info("Len arr2: %s", n2)

	n_zeroes = n1
	logging.info("Len Zeroes of in arr2: %s", n_zeroes)

	p_zeroes = n2-1
	p_arr2 = n1-1
	p_arr1 = n1-1
	while p_zeroes >= 0:
		logging.info("Start p_arr1: %s", p_arr1)
		logging.info("Start p_arr2: %s", p_arr2)
		logging.info("Start p_zeroes: %s", p_zeroes)
		if p_arr1 < 0:
			logging.info("Breaking")
			break
		elif p_arr2 < 0:
			arr2[p_zeroes] = arr1[p_arr1]
			p_arr1 -= 1
		elif arr1[p_arr1] > arr2[p_arr2]:
			logging.info("Inside if")
			arr2[p_zeroes] = arr1[p_arr1]	
			p_arr1 -= 1
		else:
			logging.info("Inside else")
			arr2[p_zeroes] = arr2[p_arr2]
			p_arr2 -= 1

		logging.info("Update arr2: %s", arr2)
		p_zeroes -= 1
		
	logging.info("Final arr2: %s", arr2)
	logging.info("Finished loop")

arr1 = [1, 3, 5]
arr2 = [2, 4, 6, 0, 0, 0]

merger_first_into_second(arr1, arr2)