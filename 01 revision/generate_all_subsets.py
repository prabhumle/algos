
# https://leetcode.com/problems/subsets/solution/
def generate_all_subsets(s):

    results = []

    def helper(slate, i):
        # Base class - leaf node
        if i == len(s):
            print(i)
            print(s[i:])
            results.append(''.join(slate))
        else:
            # Include the item in combination
            slate.append(s[i])
            print("---- Include")
            print(slate)
            helper(slate, i+1)
            slate.pop()
            print("---- Exclude")
            print(slate)
            helper(slate, i+1)

    helper([], 0)
    print(results)
    return results

generate_all_subsets("xy")
