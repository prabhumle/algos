class TreeNode():
    def __init__(self, label=None, left_ptr=None, right_ptr=None):
        self.label = label
        self.left_ptr = left_ptr
        self.right_ptr = right_ptr


root = TreeNode(label=0)
root.left_ptr = TreeNode(label=1)
root.right_ptr = TreeNode(label=2)
root.left_ptr.left_ptr = TreeNode(label=3)
root.left_ptr.right_ptr = TreeNode(label=4)
root.left_ptr.left_ptr.left_ptr = TreeNode(label=5)


class TreeNode2:
    def __init__(self, val=None, left_ptr=None, right_ptr=None):
        self.val = val
        self.left_ptr = left_ptr
        self.right_ptr = right_ptr
"""
root2 = TreeNode2(val=0)
root2.left_ptr = TreeNode2(val=1)
root2.right_ptr = TreeNode2(val=2)
root2.left_ptr.left_ptr = TreeNode2(val=3)
root2.left_ptr.right_ptr = TreeNode2(val=4)
root2.left_ptr.left_ptr.left_ptr = TreeNode2(val=5)
"""
"""
root2 = TreeNode2(val=49)
root2.left_ptr = TreeNode2(val=9092)
root2.right_ptr = TreeNode2(val=7197)
root2.left_ptr.left_ptr = TreeNode2(val=7032)
root2.left_ptr.right_ptr = TreeNode2(val=9295)
"""
root2 = TreeNode2(val=1)
root2.left_ptr = TreeNode2(val=2)
root2.right_ptr = TreeNode2(val=3)
root2.left_ptr.left_ptr = TreeNode2(val=4)
root2.left_ptr.right_ptr = TreeNode2(val=5)
root2.left_ptr.left_ptr.left_ptr = TreeNode2(val=6)
root2.left_ptr.left_ptr.right_ptr = TreeNode2(val=7)


def preorder(root):
    # Recursive
    results = []
    if not root:
        return results

    def helper(node):
        results.append(node.label)
        if node.left_ptr:
            helper(node.left_ptr)
        if node.right_ptr:
            helper(node.right_ptr)
        return results

    return helper(root)


def preorder_iterative(root):
    # Iterative
    stack = [root]
    result = []
    while stack:
        node = stack.pop()
        result.append(node.label)
        if node.right_ptr:
            stack.append(node.right_ptr)
        if node.left_ptr:
            stack.append(node.left_ptr)
    return result
#print(preorder_iterative(root))


def inorder(root):
    # RECURSIVE OPTION
    result = []
    if not root:
        return result

    def helper(node):
        if node.left_ptr:
            helper(node.left_ptr)
        result.append(node.label)
        if node.right_ptr:
            helper(node.right_ptr)
        return result

    return helper(root)


def inorder_iterative(root):
    # Iterative
    result = []
    stack = []
    pointer = root
    while True:
        if pointer:
            stack.append(pointer)
            pointer = pointer.left_ptr
        elif len(stack) > 0:
            node = stack.pop()
            result.append(node.label)
            pointer = node.right_ptr
        else:
            break
    return result
#print(inorder_iterative(root))


def postorder(root):
    # Recursive
    result = []
    if not root:
        return result

    def helper(node):
        if node.left_ptr:
            helper(node.left_ptr)
        if node.right_ptr:
            helper(node.right_ptr)
        result.append(node.label)
        return result

    return helper(root)


def postorder_iterative(root):
    # Iterative
    result = []
    stack = [root]
    stack_final = []

    while len(stack) > 0:
        node = stack.pop()
        stack_final.append(node.label)

        if node.left_ptr:
            stack.append(node.left_ptr)
        if node.right_ptr:
            stack.append(node.right_ptr)

    while len(stack_final) > 0:
        result.append(stack_final.pop())

    return result

#print(postorder_iterative(root))


from collections import deque

def level_order_traversal(root):
    result = []
    level = 0
    q = deque([(root, level)])

    while len(q) > 0:
        node, setlevel = q.popleft()
        if setlevel == level:
            interim_result.append(node.label)
        else:
            result.append(interim_result)
            interim_result = [node.label]

        if node.left_ptr:
            q.append((node.left_ptr, setlevel+1))
        if node.right_ptr:
            q.append((node.right_ptr, setlevel+1))

    return result


from collections import deque
def level_order_traversal(root):

    q = deque([root])
    main_result = []

    while len(q) > 0:
        level_result = []
        q_size = len(q)

        while q_size > 0:
            node = q.popleft()
            q_size -= 1
            level_result.append(node.label)

            if node.left_ptr:
                q.append(node.left_ptr)
            if node.right_ptr:
                q.append(node.right_ptr)

        main_result.append(level_result)
    return main_result
#print(level_order_traversal(root))



from collections import deque
class TreeNode:
    def __init__(self, _label):
        self.label = _label
        self.children = []
def level_order(root):
    # N -Ary
    result = []
    q = deque([root])

    while len(q) > 0:
        level_queue_size = len(q)
        level_nodes = []
        while level_queue_size > 0:
            node = q.popleft()
            level_nodes.append(node.label)
            # Add children of node to back of queue
            q = q + node.children
            level_queue_size -= 1
        # End of loop - Add results in this level to main result
        result.append(level_nodes)
    return result


from collections import deque
def reverse_level_order_traversal(root):
    result = []
    q = deque([root])
    stack = []

    while len(q) > 0:
        q_size = len(q)
        level_nodes = []
        while q_size > 0:
            node = q.popleft()
            level_nodes.append(node.label)
            if node.left_ptr:
                q.append(node.left_ptr)
            if node.right_ptr:
                q.append(node.right_ptr)
            q_size -= 1
        stack.append(level_nodes)

    while len(stack) > 0:
        result.append(stack.pop())
    return result

from collections import deque

def zigzag_level_order_traversal(root):
    result = []
    q = deque([root])

    level = 1
    while len(q) > 0:
        q_size = len(q)
        level_results = []
        while q_size > 0:
            node = q.popleft()
            level_results.append(node.label)
            if node.left_ptr:
                q.append(node.left_ptr)
            if node.right_ptr:
                q.append(node.right_ptr)
            q_size -= 1
        if level % 2 == 0:
            level_results.reverse()
        result.append(level_results)
        level += 1
    return result


def zigzag_level_order_traversal(root):
    result = []
    q = deque([root])

    level = 1
    while len(q) > 0:
        q_size = len(q)
        level_results = []
        while q_size > 0:
            node = q.popleft()
            level_results.append(node.label)
            if node.left_ptr:
                q.append(node.left_ptr)
            if node.right_ptr:
                q.append(node.right_ptr)
            q_size -= 1
        if level % 2 == 0:
            level_results.reverse()
        result.append(level_results)
        level += 1
    return result


def binary_tree_diameter(root):
    # For each node we will caculate the depth of the left tree and right tree
    # The node which has the maximum sum will be the diameter of the entire binary tree
    max_val = [0]

    def helper(node):
        if not node:
            return 0
        print("node:", node.label)
        left_depth = helper(node.left_ptr)
        right_depth = helper(node.right_ptr)

        max_val[0] = max(max_val[0], left_depth+right_depth)
        return max(left_depth, right_depth) + 1

    helper(root)
    return max_val[0]

#print(binary_tree_diameter(root))


class TreeNode2():
    def __init__(self, val=None, left_ptr=None, right_ptr=None):
        self.val = val
        self.left_ptr = left_ptr
        self.right_ptr = right_ptr

class SinglyLinkedListNode:
    def __init__(self, node_data):
        self.data = node_data
        self.next = None
def sorted_list_to_bst(head):
    """
        Args:
         head(SinglyLinkedListNode_int32)
        Returns:
         TreeNode_in32
    """
    # Fetch linked list into a list (already sorted)
    sorted_list = []
    while head:
        sorted_list.append(head.data)
        head = head.next

    def helper(start, end):
        if start > end:
            # No more items pending possible
            return None

        if start == end:
            root = TreeNode2(val=sorted_list[start])
            return root

        mid = (start+end)//2
        root = TreeNode2(val=sorted_list[mid])
        root.left_ptr = helper(start, mid-1)
        root.right_ptr = helper(mid+1, end)
        return root

    return helper(0, len(sorted_list)-1)


def all_paths_sum_k(root, k):
    results = []

    def dfs(node, slate, summed):
        print("slate:", slate)
        print("summed:", summed)

        if node:
            print("node.label:", node.val)
            slate.append(node.val)
            summed += node.val
            if node.left_ptr:
                dfs(node.left_ptr, slate, summed)
            if node.right_ptr:
                dfs(node.right_ptr, slate, summed)
            if not node.right_ptr and not node.left_ptr:
                dfs(None, slate, summed)
            slate.pop()
        else:
            if summed == k:
                results.append(slate[:])

    dfs(root, [], 0)
    if len(results) == 0:
        return [[-1]]
    return results

#print(all_paths_sum_k(root2, 5))



def build_binary_search_tree(preorder):

    # We wil continuously track the smallest and biggest node in each subtree
    # Based on the value is greater or less than that parent, we will move the values to left or right subtree
    # https://leetcode.com/problems/construct-binary-search-tree-from-preorder-traversal/solution/
    i = 0

    def dfs(smallest, biggest):
        nonlocal i
        if i == len(preorder):
            return None
        val = preorder[i]

        if val < smallest or val > biggest:
            return None
        i += 1

        root = TreeNode(label=val)
        root.left_ptr = dfs(smallest, val)
        root.right_ptr = dfs(val, biggest)

        return root

    return dfs(-1*pow(10, 10), pow(10, 10))





from collections import deque
def right_view(root):

    result = []
    q = deque([root])

    while len(q) > 0:
        size_q = len(q)
        same_level_nodes = []
        while size_q > 0:
            node = q.popleft()
            if node.left_ptr:
                q.append(node.left_ptr)
            if node.right_ptr:
                q.append(node.right_ptr)
            same_level_nodes.append(node.label)
            size_q -= 1
        result.append(same_level_nodes[-1])
    return result


def path_sum(root, k):
    status = [False]

    def dfs(node, summed):

        if status[0] == True:
            return

        if not node:
            if summed == k:
                status[0] = True
            return
        else:
            if node.left_ptr:
                dfs(node.left_ptr, summed+node.val)
            if node.right_ptr:
                dfs(node.right_ptr, summed+node.val)
            if not node.left_ptr and not node.right_ptr:
                dfs(None, summed+node.val)

    dfs(root, 0)
    return status[0]





def isBST(root):

    is_bst_flag = [True]

    def dfs(node, smallest, biggest):

        if not is_bst_flag[0]:
            return is_bst_flag[0]

        if not node:
            return is_bst_flag[0]

        val = node.val
        if val < smallest or val > biggest:
            is_bst_flag[0] = False
        else:
            dfs(node.left_ptr, smallest, val)
            dfs(node.right_ptr, val, biggest)
        return is_bst_flag[0]

    return dfs(root, -1*pow(10, 10), pow(10, 10))



def findSingleValueTrees(root):
    # Better simple solution here: https://leetcode.com/problems/count-univalue-subtrees/solution/
    subtree_unival = True
    subtree_not_unival = False

    def dfs(node, count):

        # Empty node base condition
        if not node:
            return (count, subtree_unival)

        # Leaf node base condition
        if not node.left_ptr and not node.right_ptr:
            count += 1
            return (count, subtree_unival)


        left_count, is_subtree_unival_left = dfs(node.left_ptr, count)
        right_count, is_subtree_unival_right = dfs(node.right_ptr, count)

        count += (left_count + right_count)

        is_subtree_unival = True
        if not is_subtree_unival_left or  not is_subtree_unival_right:
            is_subtree_unival = False
            return (count, is_subtree_unival)

        if node.left_ptr and node.right_ptr:
            if node.val==node.left_ptr.val and node.val==node.right_ptr.val:
                count += 1
            else:
                is_subtree_unival = False
        elif node.right_ptr and node.val==node.right_ptr.val:
            count += 1
        elif node.left_ptr and node.val==node.left_ptr.val:
            count += 1
        else:
            is_subtree_unival = False
        return (count, is_subtree_unival)

    result, _ = dfs(root, 0)
    return result


def allPathsOfABinaryTree(root):

    results = []
    if not root:
        return []

    def dfs(node, slate):

        slate.append(node.val)

        if not node.left_ptr and not node.right_ptr:
            results.append(slate[:])
            slate.pop()
            return


        if node.left_ptr:
            dfs(node.left_ptr, slate)
        if node.right_ptr:
            dfs(node.right_ptr, slate)
        slate.pop()

    dfs(root, [])
    return results


def flipUpsideDown(root):
    """Complete this function.
    Args:
        root (TreeNode): Root of the input tree
    Returns:
        TreeNode: Root of the output tree
    """
    # We know each left node has a subtree except for the leaf node, and all right nodes are leaf nodes
    # We can do a preorder traversal and put the items in the list then create a tree
    # or
    # We can fill the stack with root, right, and left recursively; then finally create a tree by popping the stack
    if not root:
        return root
    debug_result1=[]
    debug_result2=[]

    ptr = root
    stack = []

    # Append root
    stack.append(TreeNode2(val=ptr.val))
    debug_result1.append(ptr.val)
    while ptr:
        # One of the conditions is fine, but adding two conditions here to avoid confusion
        if ptr.left_ptr or ptr.right_ptr:
            # Append right
            stack.append(TreeNode2(val=ptr.right_ptr.val))
            debug_result1.append(ptr.right_ptr.val)
            # Append left
            stack.append(TreeNode2(val=ptr.left_ptr.val))
            debug_result1.append(ptr.left_ptr.val)

        ptr = ptr.left_ptr
    print("Stack:", stack)
    # Create a new tree
    new_root = node = stack.pop()
    debug_result2.append(node.val)
    while len(stack) >= 2:
        print("Popped Stack:", stack)
        node.left_ptr = stack.pop()
        debug_result2.append(node.left_ptr.val)
        node.right_ptr = stack.pop()
        debug_result2.append(node.right_ptr.val)
        node = node.right_ptr

    print("debug_result1:", debug_result1)
    print("debug_result2:", debug_result2)
    return new_root

#print(flipUpsideDown(root2))


class BinaryTreeNode:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

root3 = BinaryTreeNode(1)
root3.left = BinaryTreeNode(2)
root3.right = BinaryTreeNode(3)
root3.left.left = BinaryTreeNode(4)
root3.left.right = BinaryTreeNode(5)
root3.left.left.left = BinaryTreeNode(6)
root3.left.left.right = BinaryTreeNode(7)

def flip_upside_down_2(root):
    """
    Args:
     root(BinaryTreeNode_int32)
    Returns:
     BinaryTreeNode_int32
    """
    # Write your code here.
    if not root:
        return root

    bkp_left = root.left
    bkp_right = root.right
    ptr = root
    ptr.left = ptr.right = None

    while bkp_left:
        next_left = bkp_left.left
        next_right = bkp_left.right

        bkp_left.right = ptr
        bkp_left.left = bkp_right
        ptr = bkp_left

        bkp_left = next_left
        bkp_right = next_right

    return ptr

#print(flip_upside_down_2(root3))



##########################################################

class TreeNodeBST():
    def __init__(self, val=None, left_ptr=None, right_ptr=None):
        self.val = val
        self.left_ptr = left_ptr
        self.right_ptr = right_ptr


root_merge_bst1 = TreeNodeBST(val=5)
root_merge_bst1.left_ptr = TreeNodeBST(val=3)
root_merge_bst1.right_ptr = TreeNodeBST(val=6)
root_merge_bst1.left_ptr.left_ptr = TreeNodeBST(val=2)
root_merge_bst1.left_ptr.right_ptr = TreeNodeBST(val=4)
root_merge_bst1.right_ptr.right_ptr = TreeNodeBST(val=7)

root_merge_bst2 = TreeNodeBST(val=8)
root_merge_bst2.left_ptr = TreeNodeBST(val=1)
root_merge_bst2.right_ptr = TreeNodeBST(val=9)


def inorder(root, slate):
    if root:
        #print("Entering root :", root.val)
        #print("root dir:", dir(root))
        #print("root type:", type(root))
        #print("root.left_ptr dir:", dir(root.left_ptr))
        #print("root.left_ptr type:", type(root.left_ptr))
        #print("root.left_ptr:", root.left_ptr.val)
        inorder(root.left_ptr, slate)
        print("root.val:", root.val)
        slate.append(root.val)
        #print("root.right_ptr:", root.right_ptr.val)
        inorder(root.right_ptr, slate)

def bst(start, end, arr):
    if start > end:
        return None
    elif start == end:
        return TreeNodeBST(val=arr[start])
    else:
        mid = (start + end)//2
        root = TreeNodeBST(arr[mid])
        root.left_ptr = bst(start, mid-1, arr)
        root.right_ptr = bst(mid+1, end, arr)
    return root


def merge_two_BSTs(root1, root2):
    # https://www.geeksforgeeks.org/merge-two-balanced-binary-search-trees/?ref=lbp
    # First lets do a inorder traversal of the two trees and get the sorted list (since they are BSTs)
    sorted_list1 = []
    inorder(root1, sorted_list1)
    print("sorted_list1:", sorted_list1)
    sorted_list2 = []
    inorder(root2, sorted_list2)
    print("sorted_list2:", sorted_list2)

    # Merge the two sorted list together in sorted order
    i = j = 0
    merged_list = []
    while i < len(sorted_list1) and j < len(sorted_list2):
        if sorted_list1[i] < sorted_list2[j]:
            merged_list.append(sorted_list1[i])
            i += 1
        else:
            merged_list.append(sorted_list2[j])
            j += 1
    # Append any pending lists
    if i < len(sorted_list1):
        merged_list = merged_list + sorted_list1[i:]
    if j < len(sorted_list2):
        merged_list = merged_list + sorted_list2[j:]

    print("merged_list:", merged_list)
    # Creat a bst using a sorted merged list
    root = bst(0, len(merged_list)-1, merged_list)
    return root

#print(merge_two_BSTs(root_merge_bst1, root_merge_bst2))

#################################################
def postorder_traversal(root):
    # Only RECURSIVE
    if not root:
        return []

    stack = [root]
    results = []
    while len(stack) >= 1:
        node = stack[-1]
        if node.left:
            stack.append(node.left)
            # Set to none so that when it checks again this is already processed
            node.left = None
        elif node.right:
            stack.append(node.right)
            # Set to none so that when it checks again this is already processed
            node.right = None
        else:
            results.append(node.value)
            stack.pop()
    return results


def postorder_traversal_slow(root):
    """
    # Only RECURSIVE
    Args:
     root(BinaryTreeNode_int32)
    Returns:
     list_int32
    """
    stack = []
    result = []

    if not root:
        return result

    def push_to_stack_left(node):
        # Push all left nodes in the stack
        while node:
            stack.append(node)
            node = node.left

    push_to_stack_left(root)

    while len(stack) > 0:
        if stack[-1].right:
            stack.append(stack[-1].right)
            push_to_stack_left(stack[-1].right)
        else:
            item = stack.pop()
            result.append(item.value)

    # Write your code here.
    return result

#############################################
class BinaryTreeNodeLca:
    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

lcaroot = BinaryTreeNodeLca(1)
lcaroot.left = BinaryTreeNodeLca(2)
lcaroot.left.left = BinaryTreeNodeLca(4)
lcaroot.left.right = BinaryTreeNodeLca(5)
lcaroot.left.right.left = BinaryTreeNodeLca(8)
lcaroot.left.right.right = BinaryTreeNodeLca(9)
lcaroot.right = BinaryTreeNodeLca(3)
lcaroot.right.left = BinaryTreeNodeLca(6)
lcaroot.right.right = BinaryTreeNodeLca(7)

lcaroot1 = BinaryTreeNodeLca(1)
lcaroot1.left = BinaryTreeNodeLca(2)

def lca2(root, a, b):
    # Write your code here.
    if not root:
        return None

    matching_value = [None]
    res = []

    def dfs(node, slate, val):
        slate.append(node.value)
        #print("Slate:", slate)
        if matching_value[0] == val:
            return
        if node.value == val:
            matching_value[0] = node.value
            #print("match returning")
            res.append(slate[:])
            return
        if node.left:
            dfs(node.left, slate, val)
        if node.right:
            dfs(node.right, slate, val)
        slate.pop()

    dfs(root, [], a)
    # Reset matching value
    matching_value = [None]
    dfs(root, [], b)
    print("res:", res)
    if len(res) !=2 or len(res[0]) == 0 or len(res[1]) == 0:
        return None
    print("res[0]:", res[0])
    print("res[1]:", res[1])

    i = len(res[0]) - 1
    while i >= 0:
        #print("i:", i)
        j = len(res[1]) - 1
        while j >= 0:
            print("j:", j)
            print("Matching: res[0][i] & res[1][j]:", res[0][i], res[1][j])
            if res[0][i] == res[1][j]:
                return res[0][i]
            j -= 1
        i -= 1
    return None

#print(lca2(lcaroot1, 1, 1))
##################################################
class TreeNodeCdll():
    def __init__(self, val=None, left_ptr=None, right_ptr=None):
        self.val = val
        self.left_ptr = left_ptr
        self.right_ptr = right_ptr

    def __str__(self):
        return "Node - Val:{0} - left {1} and right {2}".format(self.val, self.left_ptr.val, self.right_ptr.val)

cdllroot = TreeNodeCdll(val=100)
cdllroot.left_ptr = TreeNodeCdll(val=200)
cdllroot.right_ptr = TreeNodeCdll(val=300)
cdllroot.left_ptr.left_ptr = TreeNodeCdll(val=400)
cdllroot.left_ptr.right_ptr = TreeNodeCdll(val=500)

def binary_tree_to_cdll(root):

    if not root:
        return root

    stack = [root]

    # Create sentinel and pointer
    ptr = dlist_head = TreeNodeCdll()

    while len(stack) > 0:
        node = stack[-1]
        print("stack len:", len(stack))
        print("stack top:", node.val)
        if stack[-1].left_ptr:
            print("stack left ptr added:", node.left_ptr.val)
            stack.append(node.left_ptr)
            node.left_ptr = None
        else:
            node = stack.pop()
            print("stack node popped: ", node.val)
            ptr.right_ptr = node
            node.left_ptr = ptr
            ptr = node

            if node.right_ptr:
                print("stack right ptr added: ", node.right_ptr.val)
                stack.append(node.right_ptr)
                node.right_ptr = None

    print("Completed dll creation")
    # Finally convert Dlist to circular dlist
    ptr.right_ptr = dlist_head.right_ptr
    dlist_head.right_ptr.left_ptr = ptr
    # Reset sentinel
    dlist_head.left_ptr = None
    dlist_head.right_ptr = None
    return ptr.right_ptr

res = binary_tree_to_cdll(cdllroot)
vals = []
while res:
    if res.val in vals:
        break
    print(res)
    vals.append(res.val)
    res = res.right_ptr