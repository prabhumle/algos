# Leet 261 Graph valid tree
# Given n nodes and a list of edges (undirected) function to check if it's a valid tree

# A valid tree should not have any cycles in it (basically any node should not visit previously visited node)

import logging
logging.basicConfig(level=logging.DEBUG)


def main(n, edges):

    # initiate adj list
    adj_list = [[] for _ in range(n)]
    # Populate adj list
    for source, destination in edges:
        adj_list[source].append(destination)
        # For undirected we add the following line too
        adj_list[destination].append(source)
    logging.info("Created adj list")

    # Check if the nodes are visited using 1D visited array as attribute of each node
    visited = [-1] * n
    parent = [-1] * n
    components = 0

    # dfs skeleton
    def dfs_is_tree(node, components):
        visited[node] = components
        logging.info("Visited: %s", visited)
        for neighbour in adj_list[node]:
            logging.info("Checking for node: %s | neighbour: %s", node, neighbour)
            if visited[neighbour] == -1:
                parent[neighbour] = node
                logging.info("New parent: %s", parent)
                if not dfs_is_tree(neighbour, components):
                    logging.info("inside not dfs is tree in dfs")
                    return False
            else:
                logging.info("Else in dfs: parent:%s", parent)
                if parent[node] != neighbour:
                    logging.info("parent[source] (%s) != neighbour (%s)", parent[node], neighbour)
                    return False
        return True

    # If there is more than one connected component is also not a tree???
    # Outer loop
    for v in range(n):
        logging.info("Processing vertex: %s", v)
        if visited[v] == -1:
            components += 1
            if components > 1:
                return False
            logging.info("Launching DFS for v: %s: component: %s", v, components)
            if not dfs_is_tree(v, components):
                return False
    return True

#result = main(5, [[0, 1], [0, 2], [0, 3], [1, 4]])
result = main(5, [[0, 1], [1, 2], [2, 3], [1, 3], [1, 4]])
logging.info("Result is_tree: %s", result)