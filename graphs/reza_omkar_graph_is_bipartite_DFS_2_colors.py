# Leet 785 is graph bipartite
# Given an undirected graph return true if it is bipartite
# Omkar video (but using DFS with alternate colors): 2:48:00 https://uplevel.interviewkickstart.com/resource/helpful-class-video-91245-179-347-0

# We will color code alternate nodes with different colors. If the back edge is there with same color then no bipartite

import logging
logging.basicConfig(level=logging.DEBUG)


def is_bipartate_with_alternate_colors(n, edges):

    # Create adjcency list
    adj_list = [ [] for _ in range(n) ]
    for index, edges_list in enumerate(edges):
        for edge in edges_list:
            adj_list[index].append(edge)
            adj_list[edge].append(index)

    # Create visited, parent and distance 1D attributes for the vertices
    visited = [-1] * n
    parent = [-1] * n
    color = [-1] * n

    def dfs(node):
        visited[node] = 1

        if parent[node] == -1:
            color[node] = 0
        else:
            color[node] = 1 - color[parent[node]]

        for neighbor in adj_list[node]:
            if visited[neighbor] == -1:
                visited[neighbor] = 1
                parent[neighbor] = node
                if not dfs(neighbor):
                    return False
            else:
                if parent[node] != neighbor:
                    # There is a cycle
                    if color[neighbor] == color[node]:
                        # Now there are two nodes with the same color connecting each other
                        # This means the alternate nodes are in the same set - then non-bipartate
                        return False
        return True

    # outer loop
    for i in range(n):
        if visited[i] == -1:
            if not dfs(i):
                return False
    return True

edges = [[1, 3], [0, 2], [1, 3], [0,2]]
#edges = [[0, 1], [1, 2], [2, 3], [1, 3], [1, 4]]
count_vertices = len(edges)
result = is_bipartate_with_alternate_colors(count_vertices, edges)
logging.info("Bipartate: %s", result)
