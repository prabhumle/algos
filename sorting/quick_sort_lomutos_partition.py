# We will implement this logic using lomuto's partitioning where both pointers to sort will start from the beginning of array
# In Hoarse's partitioning the second pointer moves from the end of the array to until the pivot

import logging
import random
logging.basicConfig(level=logging.DEBUG)


def quick_sort(arr):
    quicksort_helper(arr, 0, len(arr)-1)
    logging.info("Final arr: %s", arr)


def quicksort_helper(arr, start, end):
    logging.info("arr: %s", arr)
    logging.info("start: %s | end: %s", start, end)
    # base case for recursion
    if start >= end:
        return
    pivot_index = random.randint(start, end)
    logging.info("pivot_index: %s", pivot_index)
    pivot = arr[pivot_index]
    logging.info("pivot: %s", pivot)
    logging.info("Before pivot swap  arr: %s", arr)
    # Move/swap the pivot to the beginning of the arr
    arr[start], arr[pivot_index] = arr[pivot_index], arr[start]
    logging.info("After pivot swap  arr: %s", arr)
    # For Lomuto's partition start the pointers from the front excluding the pivot element
    smaller = start
    bigger = start + 1
    # https://uplevel.interviewkickstart.com/resource/rc-video-56514-260862-16-141-1421364
    while bigger <= end and smaller <= end:
        logging.info("In while  arr: %s", arr)
        logging.info("pivot: %s, smaller: %s | bigger: %s | smaller val: %s | bigger val: %s", pivot, smaller, bigger, arr[smaller], arr[bigger])
        if arr[bigger] < pivot:
            # if big index is smaller, swap with the smaller index
            smaller += 1
            arr[smaller], arr[bigger] = arr[bigger], arr[smaller]
        bigger += 1

    logging.info("End while  arr: %s", arr)
    logging.info("End while smaller: %s", smaller)
    # After while swap end of smaller with pivot
    arr[smaller], arr[start] = arr[start], arr[smaller]
    logging.info("Post pivot swap  arr: %s", arr)
    # Call function recursively
    quicksort_helper(arr, start, smaller-1)
    quicksort_helper(arr, smaller+1, end)

arr = [1,2,3,8,10,7,6,6]
arr = [10, 11, 1,2,3,8,10,7,6,6]
#arr = [2, 1]
quick_sort(arr)