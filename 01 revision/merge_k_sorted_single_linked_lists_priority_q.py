"""
Merge k Sorted Singly Linked Lists
Given k singly linked lists where each linked list is sorted in ascending order, merge all of them into a single sorted linked list.
Example:
Input: [ [1 -> 3 -> 5], [3 -> 4], [7] ].
Output: 1 -> 3 -> 3 -> 4 -> 5 -> 7
"""
# https://leetcode.com/problems/merge-k-sorted-lists/solution/

import logging
logging.basicConfig(level=logging.DEBUG)
from queue import PriorityQueue

# https://stackoverflow.com/questions/53554199/heapq-push-typeerror-not-supported-between-instances

# https://stackoverflow.com/questions/40205223/priority-queue-with-tuples-and-dicts
class use_only_firs t:
    def __init__(self, value, node):
        self._value, self._node = value, node

    def __lt__(self, other):
        return self._value < other._value

class SinglyLinkedListNode:

    def __init__(self, data=0, next=None):
        self.data = data
        self.next = next

import heapq
def merge_k_lists(lists):

    head = pointer = SinglyLinkedListNode(0)  # Create a sentinel to mark the beginning of a new node

    # q = PriorityQueue()
    heap = []
    heapq.heapify(heap)
    # Fetch head of each linked list and add it to the priority queu
    for k in lists:
        #q.put((k.data, k))
        heapq.heappush(heap, use_only_first(k.data, k))
    # Space complexity: O(k) & time complexity O(k)

    #while not q.empty():
    while len(heap) > 0:
        #value, node = q.get()
        obj = heapq.heappop(heap)
        value, node = obj._value, obj._node
        # Add to the newly assembled linked list
        pointer.next = SinglyLinkedListNode(value)   # Create new LL takes O(n) time and space complexity
        pointer = pointer.next
        # For the next node in the original list put it in the queue
        if node.next:
            # This will take n puts (total n elements in all of k nodes)
            #q.put((node.next.data, node.next))
            heapq.heappush(heap, use_only_first(node.next.data, node.next))

    return head.next  # because the head always points to the sentinel


# Space complexity = o(k) + o(n) = O(n+k)
# Time complexity = O(k) + O(k+n) + O( knlog n)

ll_lists = []
for l in [[1, 3, 5], [3, 4], [7]]:
    prev_node = None
    for i in l:
        node = SinglyLinkedListNode(i)
        node.next = None
        if prev_node is None:
            prev_node = node
            head = node
        else:
            prev_node.next = node
    ll_lists.append(head)

res = merge_k_lists(ll_lists)

print("Result linked list")
while res.next:
    print(res.data)
    res = res.next
else:
    print("inside else")
    print(res.data)