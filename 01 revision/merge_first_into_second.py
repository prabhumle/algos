import logging
logging.basicConfig(level=logging.DEBUG)


def merger_first_into_second(arr1, arr2):
    # Sort the elements in reverse order by filling the 2nd half of the bigger array arr2 in desc order
    # Copy any pending items in arrays to arr2 in reverse order

    i = len(arr1)-1
    j = int(len(arr2)/2)-1
    arr2_reverse = -1

    while i >= 0 and j >= 0:
        logging.info("i: %s|j: %s|arr2_reverse: %s", i, j, arr2_reverse)
        if arr1[i] > arr2[j]:
            arr2[arr2_reverse] = arr1[i]
            i -= 1
        else:
            arr2[arr2_reverse] = arr2[j]
            j -= 1

        arr2_reverse -= 1
        logging.info("arr2 current: %s", arr2)

    while i >= 0:
        arr2[arr2_reverse] = arr1[i]
        i -= 1
        arr2_reverse -= 1
    logging.info("arr2 after i: %s", arr2)

    while j >= 0:
        arr2[arr2_reverse] = arr2[j]
        j -= 1
        arr2_reverse -= 1
    logging.info("arr2 after j: %s", arr2)
    return arr2

#arr1 = [1,3,5]
#arr2 = [2,4,6, 0, 0, 0]
arr1 = [2]
arr2 = [1, 0]
merger_first_into_second(arr1, arr2)