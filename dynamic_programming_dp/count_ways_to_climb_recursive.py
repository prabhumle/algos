import logging
logging.basicConfig(level=logging.DEBUG)
# Complete the countWaysToClimb function below.
"""
Count Ways To Reach The Nth Step
There is a staircase with n steps. A person standing at the 0-th step wants to reach the n-th one. They are capable of jumping up by certain numbers of steps at a time.
Given how the person can jump, count the number of ways they can reach the top.
"""


def countWaysToClimb(steps, n):
    # Create a 1D dp table/array with base conditions 0 for steps < MIN(steps_allowed: example:[2,3])
    min_step = min(steps)  # O(N)
    logging.info("min_step: %s", min_step)

    # Create data structure and size
    dp_table = [0] * (n+1)  # 1 is for the 0 use case
    logging.info("dp_table: %s", dp_table)

    # Set 0 steps for unreachable steps
    for i in range(0, min_step+1):
        dp_table[i] = 0
        if i == min_step and min_step <= n:
            dp_table[i] = 1
    logging.info("DP table after initialization: %s", dp_table)

    # Populate the rest of the 1D array using the recurrence equation found in the recurrence code
    for i in range(min_step+1, n+1 ):
        logging.info("Processing DP table col i: %s", i)
        sum = 0
        for step in steps:
            logging.info("Deducting step: %s i-step: %s", step, i-step)
            if i < step:
                sum += dp_table[i-step]
            logging.info("Sum so far: %s", sum)
        logging.info("steps after all deduction for i:%s: %s", i, sum)
        dp_table[i] = sum
    logging.info("DP table after population: %s", dp_table)
    return dp_table[n]



result = countWaysToClimb([1,2], 2)
#result = countWaysToClimb([2, 3], 7)
#result = countWaysToClimb([1,2], 1)
logging.info("result: %s", result)