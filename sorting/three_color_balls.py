"""
Dutch National Flag



Given some balls of three colors arranged in a line, rearrange them such that all the red balls go first, then green and then blue ones.



Do rearrange the balls in place. A solution that simply counts colors and overwrites the array is not the one we are looking for.



This is an important problem in search algorithms theory proposed by Dutch computer scientist Edsger Dijkstra. Dutch national flag has three colors (albeit different from ones used in this problem).



Example

Input: [G, B, G, G, R, B, R, G]

Output: [R, R, G, G, G, G, B, B]



There are a total of 2 red, 4 green and 2 blue balls. In this order they appear in the correct output.



Notes

Input Parameters: An array of characters named balls, consisting of letters from {‘R’, ‘G’, ‘B’}.



Output: Return type is void. Modify the input character array by rearranging the characters in-place.



Constraints:

1 <= n <= 100000
Do this in ONE pass over the array - NOT TWO passes, just one pass.
Solution is only allowed to use constant extra memory.
"""

## Does not work

import logging
logging.basicConfig(level=logging.DEBUG)
logging.debug('This will get logged')


def dutch_flag_sort(balls):
    end = len(balls) - 1    
    ptr = 0
    r_ptr = 0
    b_ptr = end
    while ptr <= b_ptr:
        logging.info("%s", balls)
        logging.info("ptr: %s | r_ptr: %s | b_ptr:%s", ptr, r_ptr, b_ptr)
        logging.info("Current ball: %s", balls[ptr])
        if balls[ptr] == "R":
            balls[r_ptr], balls[ptr] = balls[ptr], balls[r_ptr]
            ptr += 1
            r_ptr += 1
        elif balls[ptr] == "G":
            ptr += 1
        else:
			# Blue
            balls[b_ptr], balls[ptr] = balls[ptr], balls[b_ptr]
            #ptr += 1
            b_ptr -= 1
        	
        logging.info(" --- ")  
        
    print(balls)


dutch_flag_sort( ['G', 'B', 'G', 'G', 'R', 'B', 'R', 'G'])