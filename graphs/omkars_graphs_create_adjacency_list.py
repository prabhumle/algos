# https://docs.google.com/presentation/d/1-adnRYmnI-0qyffHmCgiNls8XhKc1_kFEMYOAXZ9v7g/edit#slide=id.g54afff569f_0_265
# Slide 65
import logging
logging.basicConfig(level=logging.DEBUG)


class Graph(object):
    def __init__(self, no_of_vertices):
        self.n = no_of_vertices
        self.adjList = [[] for i in range(self.n)]

    def addEdge(self, start, end, bidir=True):
        self.adjList[start].append(end)
        if bidir:
            self.adjList[end].append(end)

    def has_eulerian_cycle(self):
        """Check if there are any vertices have odd number of edges
        :return: boolean
        """
        vertices_odd_edges = 0
        for v in range(0, self.n):
            if len(self.adjList[v]) % 2 == 1:
                vertices_odd_edges += 1

        logging.info("vertices_odd_edges: %s", vertices_odd_edges)
        if vertices_odd_edges == 0:
            return True
        else:
            return False


    def has_eulerian_path(self):
        """Check if there are any vertices have 0 or 2 odd edges
        Note: Eulerian cycle is also a Eulerian path - so the condition for 0 odd edges exists
        :return: boolean
        """
        vertices_odd_edges = 0
        for v in range(0, self.n):
            if len(self.adjList[v]) % 2 == 1:
                vertices_odd_edges += 1

        logging.info("vertices_odd_edges: %s", vertices_odd_edges)
        if vertices_odd_edges == 0 or vertices_odd_edges == 2:
            return True
        else:
            return False


biddir_graph = Graph(10)
biddir_graph.addEdge(0, 1)
biddir_graph.addEdge(0, 6)
biddir_graph.addEdge(0, 8)
biddir_graph.addEdge(1, 0)
biddir_graph.addEdge(1, 4)
biddir_graph.addEdge(1, 6)
biddir_graph.addEdge(1, 9)
biddir_graph.addEdge(2, 4)
biddir_graph.addEdge(2, 6)
biddir_graph.addEdge(3, 4)
biddir_graph.addEdge(3, 5)
biddir_graph.addEdge(3, 8)
biddir_graph.addEdge(4, 1)
biddir_graph.addEdge(4, 2)
biddir_graph.addEdge(4, 3)
biddir_graph.addEdge(4, 5)
biddir_graph.addEdge(4, 9)
biddir_graph.addEdge(5, 3)
biddir_graph.addEdge(5, 4)
biddir_graph.addEdge(6, 0)
biddir_graph.addEdge(6, 1)
biddir_graph.addEdge(6, 2)
biddir_graph.addEdge(7, 8)
biddir_graph.addEdge(7, 9)
biddir_graph.addEdge(8, 0)
biddir_graph.addEdge(8, 3)
biddir_graph.addEdge(8, 7)
biddir_graph.addEdge(9, 1)
biddir_graph.addEdge(9, 4)
biddir_graph.addEdge(9, 7)

logging.info(biddir_graph.adjList)
logging.info("Has Eulerian cycle: %s", biddir_graph.has_eulerian_cycle())
logging.info("Has Eulerian path: %s", biddir_graph.has_eulerian_cycle())