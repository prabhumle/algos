"""
Given an array and a target number, find the indices of the two values from the array that sum up to the given target number.

Example One:

Input: [5, 3, 10, 45, 1], 6

Output: [0, 4]

Sum of the elements at index 0 and 4 is 6.
"""
def two_sum(numbers, target):

    left = 0
    right = len(numbers) - 1

    while left < right:
        sum_val = numbers[left] + numbers[right]

        if sum_val == target:
            return [left, right]

        left += 1
        if left == right:
            left = 0
            right -= 1

    return [-1, -1]

#print(two_sum([5, 3, 10, 45, 1], 6))
print(two_sum([-100000, -5, 6, 100000, 5, 9, 10], -10))
