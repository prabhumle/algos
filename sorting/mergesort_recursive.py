import logging
logging.basicConfig(level=logging.DEBUG)
logging.debug('This will get logged')

def merge_sort(arr):
    """
    Args:
     arr(list_int32)
    Returns:
     list_int32
    """
    # Write your code here.
    mergesort(arr, 0, len(arr)-1)
    print(str(arr))
    return arr
    
def merge(arr, start, mid, end):
    aux_array = []
    i = start
    j = mid+1
    logging.info("Merging aux_array 1:%s", aux_array)
    logging.info("Merging: arr: %s", arr)
    logging.info("Merging: start: %s end: %s mid: %s", start, end, mid)
    logging.info("Merging: i: %s, j:%s", i, j)
    while i <= mid and j <= end:
        if arr[i] <= arr[j]:
            logging.info("Inside if")
            aux_array.append(arr[i])
            i+=1
        else:
            logging.info("Inside else")
            aux_array.append(arr[j])
            j+=1
    logging.info("Merging aux_array 2:%s", aux_array)
    # Check if any left over arrays are left and append those
    logging.info("Remaining Merging: i: %s, j:%s", i, j)
    while i <= mid:
        aux_array.append(arr[i])
        i+=1
    while j <= end:
        aux_array.append(arr[j])
        j+=1
    logging.info("Merging aux_array 3:%s", aux_array)
    # Add items captured in aux array to original array
    logging.info("-->Merging aux: start: %s end: %s", start, end)
    arr[start:end+1] = aux_array
    logging.info("Merge: %s", arr)
    
def mergesort(arr, start, end):
    if start == end:
        logging.info("Reached end - recursive- start: %s, end: %s", start, end)
        return
    mid = int((start + end)/2)
    logging.info("Start: %s End: %s Mid: %s", start, end, mid)

    # Update left array
    logging.info("Updating left")
    mergesort(arr, start, mid)
    # Update right array
    logging.info("Updating right")
    mergesort(arr, mid+1, end)
    # merge
    merge(arr, start, mid, end)
    
    return arr
        
    
merge_sort([3,2,1,4,5,6,7])