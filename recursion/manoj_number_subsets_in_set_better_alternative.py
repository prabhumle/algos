# Memory optimization - use a single slate
# In the same previous implementation - slate is creating atleast n additional memory by creating new slates
# Here will reuse the slate for better memory usage (without slicing or splicing operations)

import logging
logging.basicConfig(level=logging.DEBUG)

# Example: Input arr = [1,2,3]
#result = [{}, {1}, {2}, {3}, {1,2}, {1,3}, {2,3}, {1,2,3}]

import logging
logging.basicConfig(level=logging.DEBUG)


def find_number_of_subsets(arr):
    result = []
    slate_ids = []

    def helper(slate, i):
        logging.info("slate: %s | i: %s", slate, i)
        logging.debug("Top: Slate id-------------------> %s", id(slate))
        slate_ids.append(id(slate))
        # Base case
        if i == len(arr):
            logging.info("Appending slate: %s to result: %s", slate, result)
            result.append(slate[:])
            return
        else:
            # Include
            logging.info("Include i:%s", i)
            slate.append(arr[i])  # Append to the same slate
            helper(slate, i+1)
            logging.debug("In 1: Slate id-------------------> %s", id(slate))
            slate.pop()  # Pop the last element after the exclude operation
            # Exclude
            logging.info("Exclude i:%s", i)
            helper(slate, i+1)
            logging.debug("In 2: Slate id-------------------> %s", id(slate))


    helper([], 0)
    logging.info("result: %s", result)
    logging.info("slate_id count: %s, slate_ids: %s", len(set(slate_ids)), set(slate_ids))

arr = [1,2,3]
find_number_of_subsets(arr)
