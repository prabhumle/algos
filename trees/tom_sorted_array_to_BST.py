# The live class was from Tom Gilmour but the logic is from Omkar's PPT
# https://drive.google.com/file/d/1pyDINp9utJNG2uifRRGneRpg5Q5FPolu/view


# Leet# 108 convert sorted array to binary search Tree (BST)

import logging
logging.basicConfig(level=logging.DEBUG)


# The node creation logic is simply based on reference from https://www.geeksforgeeks.org/binary-tree-set-1-introduction/
class TreeNode(object):
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None

input_arr = [-10, -3, 0, 5, 9]

def BST(input_arr):

    def helper(A, start, end):
        logging.info("Start: %s | end: %s", start, end)
        if start > end:
            logging.info("In base class")
            return None
        elif start == end:
            logging.info("Start == end: Setting node ===>: %s", A[start])
            return TreeNode(A[start])
        else:
            mid = int((start+end)/2)
            logging.info("Mid: %s", mid)
            root_node = TreeNode(A[mid])
            logging.info("Set node:===> %s", A[mid])
            logging.info("Setting left")
            root_node.left = helper(A, start, mid-1)
            logging.info("Setting right")
            root_node.right = helper(A, mid+1, end)
            logging.info("Returning node")
            return root_node

    helper(input_arr, 0, len(input_arr)-1)

BST(input_arr)

