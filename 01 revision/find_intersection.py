
#
# Complete the function below.
#
# The function accepts three INTEGER_ARRAY arr1, arr2, arr3 as parameter and expected to return an INTEGER_ARRAY.
#

def find_intersection(arr1, arr2, arr3):
    # Write your code here
    a1 = a2 = a3 = 0
    result = []
    while a1 < len(arr1) and a2 < len(arr2) and a3 < len(arr3):

        if arr1[a1] == arr2[a2] and arr1[a1] == arr3[a3]:
            result.append(arr1[a1])
            a1 += 1
            a2 += 1
            a3 += 1
        else:
            if arr1[a1] < arr2[a2]:
                a1 += 1
            elif arr2[a2] < arr3[a3]:
                a2 += 1
            else:
                a3 += 1
    if len(result) == 0:
        return [-1]
    return result


def find_intersection_not_clean(arr1, arr2, arr3):
    # Write your code here
    a1 = a2 = a3 = 0
    result = []
    while a1 < len(arr1) and a2 < len(arr2) and a3 < len(arr3):

        if arr1[a1] == arr2[a2] and arr1[a1] == arr3[a3]:
            result.append(arr1[a1])
            a1 += 1
            a2 += 1
            a3 += 1
        else:
            if (arr1[a1] < arr2[a2]) or (arr1[a1] < arr3[a3]) :
                a1 += 1
            elif (arr2[a2] < arr1[a1]) or (arr2[a2] < arr3[a3]):
                a2 += 1
            elif (arr3[a3] < arr1[a1]) or (arr3[a3] < arr2[a2]):
                a3 += 1
    if len(result) == 0:
        return [-1]
    return result


arr1 = [2, 5, 10]
arr2 = [2, 3, 4, 10]
arr3 = [2, 4, 10]
find_intersection(arr1, arr2, arr3)