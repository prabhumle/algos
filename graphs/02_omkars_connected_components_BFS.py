#https://docs.google.com/presentation/d/1-adnRYmnI-0qyffHmCgiNls8XhKc1_kFEMYOAXZ9v7g/edit#slide=id.g54afff569f_0_1061
# Slide 95

# Import bidirectional graph created in the first module
from omkars_graphs_create_adjacency_list import *
from collections import deque

visited = [-1] * biddir_graph.n
parent = [-1] * biddir_graph.n


def BFS(node, connected_component_counter):
    q = deque([node])
    visited[node] = connected_component_counter
    while len(q) !=0:
        current_node = q.popleft()
        # Go through all the immediate neighbours in graph and add them to queue
        for neighbour in biddir_graph.adjList[current_node]:
            if visited[neighbour] == -1:
                q.append(neighbour)
                parent[neighbour] = current_node
                visited[neighbour] = connected_component_counter
    logging.info("BFS visited so far: %s", visited)


# Outer loop
# Create a graph for every vertex provided and marking unvisited nodes (Vertexes) with the appropriate counter
connected_component_counter = 0
for vertex in range(0, biddir_graph.n):
    if visited[vertex] == -1:
        BFS(vertex, connected_component_counter)
        connected_component_counter += 1
logging.info("connected_components: %s", connected_component_counter)
logging.info("visited: %s", visited)
logging.info("parent: %s", parent)




