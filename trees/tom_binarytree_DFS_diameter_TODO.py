# The live class was from Tom Gilmour but the logic is from Omkar's PPT
# https://drive.google.com/file/d/1pyDINp9utJNG2uifRRGneRpg5Q5FPolu/view


# Leet# 543 The diameter is the longest path between two nodes (which may or may not include root)

import logging
logging.basicConfig(level=logging.DEBUG)


# The node creation logic is simply based on reference from https://www.geeksforgeeks.org/binary-tree-set-1-introduction/
class TreeNode(object):
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None


# Create a tree provided in the problem Leet#102
root = TreeNode(1)
root.left = TreeNode(2)
root.right = TreeNode(3)
root.left.left = TreeNode(4)
root.left.right = TreeNode(5)


# Solution
def dfs(node):
    if node is None:
        return 0

    max_diameter = [0]

    def helper(node):
        diameter = 0
        logging.info("node: %s =======================> Starting helper with  diameter 0", node.val)
        if node.left is None and node.right is None:
            logging.info("Reached leaf node !!!!!!")
            pass
        else:
            logging.info("In Else")
            if node.left:
                left_diameter = helper(node.left)
                logging.info("LEFT - Diameter at node: %s is %s", node.left.val, left_diameter)
                diameter += 1
                diameter += left_diameter
                logging.info("Incremented diameter left: %s", diameter)
            if node.right:
                right_diameter = helper(node.right)
                logging.info("RIGHT - Diameter at node: %s is %s", node.right.val, right_diameter)
                diameter += 1
                diameter += right_diameter
                logging.info("Incremented diameter right: %s", diameter)

        logging.info("Diameter: %s | Max diameter available: %s", diameter, max_diameter)
        if diameter > max_diameter[0]:
            logging.info("Updating max diameter")
            max_diameter[0] = diameter
        return max_diameter[0]

    helper(node)

dfs(root)

